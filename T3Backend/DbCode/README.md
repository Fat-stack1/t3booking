# POSTMAN
#### *../DbCode/demodb.sql*
*For more information on how to use PostMan and understand our project structure:* 
```        
https://www.bezkoder.com/spring-boot-jwt-authentication/
```
### index

```
1. POST - signup
    1.1 ADMIN
    1.2 EMPLOYEE
    1.3 PRIVATE CUSTOMER
    1.4 CORPORATE CUSTOMER
2. POST - sign-in
    2.1 ADMIN
    2.2 EMPLOYEE
    2.3 PRIVATE CUSTOMER
    2.4 CORPORATE CUSTOMER
3. GET - Access resources
    3.1 Public content
    3.2 Protected content
    3.3 ROLE_CUSTOMER content
    3.4 ROLE_EMPLOYEE content
    3.5 ROLE_ADMIN content
4. Other
    4.1 SELECT x FROM - (isCorporate)
```

## 1. POST - signup
```
POST /api/auth/signup           http://localhost:8080/api/auth/signup

Body:
    "accessToken": "..."
```

#### 1.1 ADMIN:
```
{
    "username": "admin",
    "password": "123",
    "email": "admin@demo.se",
    "role": ["admin"]
}
```
#### 1.2 EMPLOYEE:
```
{
    "username": "emp",
    "password": "123",
    "email": "employee@demo.se",
    "role": ["employee"]
}
```
#### 1.3 PRIVATE CUSTOMER:
```
{
    "username": "priv",
    "password": "123",
    "email": "private@demo.se",
    "role": ["customer"]
}
```
#### 1.4 CORPORATE CUSTOMER:
```
{
    "username": "corp",
    "password": "123",
    "email": "corporate@demo.se",
    "role": ["customer"]
}
```

## 2. POST - sign-in
*The response body will display the json object, where "accessToken" will be used for role-access, see section 3.2 GET access ROLE_x*
```
POST /api/auth/sign-in          http://localhost:8080/api/auth/sign-in

Body:
    "accessToken": "..."
```

#### 2.1 ADMIN:
```
{
    "username": "admin",
    "password": "123"
}
```
#### 2.2 EMPLOYEE:
```
{
    "username": "emp",
    "password": "123"
}
```
#### 2.3 PRIVATE CUSTOMER:
```
{
    "username": "priv",
    "password": "123"
}
```
#### 2.4 CORPORATE CUSTOMER:
```
{
    "username": "corp",
    "password": "123"
}
```

## 3. GET - Access resources
#### 3.1 Public content:
```
GET /api/test/all              http://localhost:8080/api/test/all
```
#### 3.2 Protected content:
```
GET /api/test/customer              http://localhost:8080/api/test/customer
```
#### 3.3 ROLE_CUSTOMER content:
```
GET /api/test/customer              http://localhost:8080/api/test/customer

Headers:
    Key = Authorization
    Value = Bearer (accessToken)
```
#### 3.4 ROLE_EMPLOYEE content:
```
GET /api/test/employee              http://localhost:8080/api/test/employee

Headers:
    Key = Authorization
    Value = Bearer (accessToken)
```
#### 3.5 ROLE_EMPLOYEE content:
```
GET /api/test/admin              http://localhost:8080/api/test/admin

Headers:
    Key = Authorization
    Value = Bearer (accessToken)
```

## 4. Other
#### 4.1 SELECT x FROM:
*To show true/false instead of 1 or 0 in UserRow column 'isCorporate', run the following command:*
```
SELECT 
    id, 
    firstname, 
    lastname, 
    phone, 
    address, 
    postcode, 
    city, 
IF(isCorporate, 'true', 'false') 
    isCorporate FROM t3data.user_details;
```