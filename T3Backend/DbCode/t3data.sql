DROP DATABASE IF EXISTS `t3data`;
CREATE DATABASE /*IF NOT EXISTS*/ `t3data`;
USE t3data;

INSERT INTO `t3data`.`status` (`name`)
VALUES ('BOOKED'),
       ('UNCONFIRMED'),
       ('CONFIRMED'),
       ('IN_PROGRESS'),
       ('PERFORMED'),
       ('APPROVED'),
       ('DISAPPROVED'),
       ('INVOICED'),
       ('PAYED');

### KAN TAS BORT BEHÖVS INTE LÄNGRE
###>>>>
#
# /************ROLE TABLE***************/
# /* Existing roles */
#
# DROP TABLE IF EXISTS `role`;
# CREATE TABLE `role`
# (
#     `id`   int NOT NULL AUTO_INCREMENT,
#     `name` varchar(20),
#     PRIMARY KEY (`id`)
# );
#
# /* Adding roles to table */
#
# ### KAN TAS BORT BEHÖVS INTE LÄNGRE.
# INSERT INTO `t3data`.`role` (`name`)
# VALUES ('ROLE_ADMIN'),
#        ('ROLE_EMPLOYEE'),
#        ('ROLE_CUSTOMER');
#
#
# /************CLEANING_SERVICES TABLE***************/
# /* Available cleaning services TODO: Rut & rot istället för msrp med relaistiska priser? */
#
#
# DROP TABLE IF EXISTS `cleaning_services`;
# CREATE TABLE `cleaning_services`
# (
#     `id`    int            NOT NULL AUTO_INCREMENT,
#     `name`  varchar(50)    NOT NULL,
#     `price` decimal(10, 2) NOT NULL,
#     `msrp`  decimal(10, 2) NOT NULL,
#     PRIMARY KEY (`id`)
# );
#
# INSERT INTO `t3data`.`cleaning_services` (`name`, `price`, `msrp`)
# VALUES ('Basic_Städning', '249.12', '199.32'),
#        ('Topp_Städning', '289.05', '259.29'),
#        ('Diamant_Städning', '239.66', '296.94'),
#        ('Fönstertvätt', '127.84', '99.73');
#
# alter table booking modify date date null;
###<<<<<<
