package com.t3.stadafint.repositories;

import com.t3.stadafint.entities.Role;
import com.t3.stadafint.entities.User;
/*import org.apache.catalina.Role;
import org.apache.catalina.User;*/
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    List<User> findUserByRoles(Optional<Role> roles);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}

