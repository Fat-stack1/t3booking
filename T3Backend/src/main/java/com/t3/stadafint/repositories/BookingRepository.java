package com.t3.stadafint.repositories;

import com.t3.stadafint.entities.Booking;
import com.t3.stadafint.entities.EStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findByDate(String date);

    @Query("select booking from Booking booking where booking.customer_id = ?1")
    List<Booking> findBookingsByCustomer_id(Long customer_id);

    @Query("select booking from Booking booking where booking.employee_id = ?1")
    List<Booking> findBookingsByEmployee_id(Long employee_id);

    @Query("select b from Booking b where b.customer_id = ?1 and b.status = ?2")
    List<Booking> findByCustomer_idAndStatus(Long customer_id, EStatus status);

    @Query("select b from Booking b where b.employee_id = ?1 and b.status = ?2")
    List<Booking> findByEmployee_idAndStatus(Long employee_id, EStatus status);

    @Query("select b from Booking b where b.status = ?1")
    List<Booking> findByStatus(EStatus status);
}
