package com.t3.stadafint.repositories;


import com.t3.stadafint.entities.CleaningService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CleaningServiceRepository extends JpaRepository<CleaningService, Long> {}
