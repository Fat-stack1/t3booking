package com.t3.stadafint.payload.response;

import com.sinch.xms.ApiConnection;
import com.sinch.xms.SinchSMSApi;
import com.sinch.xms.api.MtBatchTextSmsCreate;
import com.sinch.xms.api.MtBatchTextSmsResult;

public class SmsResponse {
    private static final String SERVICE_PLAN_ID = "886cefa27db14fae8b8f30c90a6180c4";
    private static final String TOKEN = "0cbef937cab144909b2b8eff886f5e68";
    private static final ApiConnection conn = null;

    public static void smsMain() {
//        String SENDER = "447520651416";
        String SENDER = "Städa Fint AB";
        String[] RECIPIENTS = {"46703580312"}; // Skriv ditt nummer för att testa tjänsten ! :D

        ApiConnection conn = ApiConnection.builder().servicePlanId(SERVICE_PLAN_ID).token(TOKEN).start();

        MtBatchTextSmsCreate message = SinchSMSApi.batchTextSms().sender(SENDER).addRecipient(RECIPIENTS)
                .body("DUMMY BEKRÄFTELSE : ddmmyyyy Tack för du valde Städa Fint AB!").build();
        try {
            MtBatchTextSmsResult batch = conn.createBatch(message);
            System.out.println(batch.id());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("you sent: " + message.body());
    }
}

