package com.t3.stadafint.payload.request;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class BookingRequest {
    private Long customer_id;
    private Long cleaning_service_id;
    private Date date;
    private String time;
}
