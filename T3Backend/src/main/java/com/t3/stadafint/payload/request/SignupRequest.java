package com.t3.stadafint.payload.request;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter

public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private String firstname;
    private String lastname;
    private String phone;
    private String address;
    private String postalcode;
    private String city;
    private Set<String> role;

    @NotBlank
    @Size(min = 3, max = 40)
    private String password;

}
