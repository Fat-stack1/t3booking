package com.t3.stadafint.payload.response;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String username;
    private String email;
    private String firstname;
    private String lastname;
    private String phone;
    private String address;
    private String postalcode;
    private String city;
    private List<String> roles;

    public JwtResponse(String token, Long id, String username, String email, String firstname, String lastname, String phone, String address, String postalcode, String city, List<String> roles) {
        this.token = token;
        this.id = id;
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.address = address;
        this.postalcode = postalcode;
        this.city = city;
        this.roles = roles;
    }
}
