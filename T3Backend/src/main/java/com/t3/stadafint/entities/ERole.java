package com.t3.stadafint.entities;

public enum ERole {
    ROLE_CUSTOMER,
    ROLE_ADMIN,
    ROLE_EMPLOYEE
}
