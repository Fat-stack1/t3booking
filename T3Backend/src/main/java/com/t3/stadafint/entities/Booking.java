package com.t3.stadafint.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "customer_id")
    private Long customer_id;

    @Column(name = "employee_id")
    private Long employee_id;

    @Column(name = "cleaning_service_id")
    private Long cleaning_service_id;

    @Column(name = "date", columnDefinition = "date")
    private Date date;

    @Column(name = "time")
    private String time;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EStatus status;

    @Column(name="timestamp")
    private Timestamp timestamp;

    @Column(name = "comment")
    private String comment;

    public Booking(Long customer_id, Long employee_id, Long cleaning_service_id, Date date, String time, Timestamp timestamp, EStatus status, String comment) {
        this.customer_id = customer_id;
        this.employee_id = employee_id;
        this.cleaning_service_id = cleaning_service_id;
        this.date = date;
        this.time = time;
        this.timestamp = timestamp;
        this.status = status;
        this.comment = comment;
    }
}
