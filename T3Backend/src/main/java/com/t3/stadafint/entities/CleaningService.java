package com.t3.stadafint.entities;

import lombok.Getter;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "cleaning_services")
public class CleaningService {
    @Id
    private Long id;
    private String name;
    private float price;
    private float msrp;
}
