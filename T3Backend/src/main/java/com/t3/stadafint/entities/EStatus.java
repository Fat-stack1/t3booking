package com.t3.stadafint.entities;

public enum EStatus {
    OBEKRÄFTAD,
    BEKRÄFTAD,
    BOKAD,
    UNDER_UTFÖRANDE,
    UTFÖRT,
    GODKÄNT,
    FAKTURERAD,
    BETALD,
    UNDERKÄND
}
