package com.t3.stadafint.controllers;

import com.t3.stadafint.entities.Booking;
import com.t3.stadafint.entities.CleaningService;
import com.t3.stadafint.entities.EStatus;
import com.t3.stadafint.payload.request.BookingRequest;
import com.t3.stadafint.payload.response.MessageResponse;
import com.t3.stadafint.payload.response.SmsResponse;
import com.t3.stadafint.repositories.BookingRepository;
import com.t3.stadafint.repositories.CleaningServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/t3")
public class  BookingController {

    @Autowired
    private CleaningServiceRepository cleaningServiceRepository;

    @Autowired
    private BookingRepository bookingRepository;

    @GetMapping("/cleanings")
    public List<CleaningService> fetchCleaningOptions(){
        return cleaningServiceRepository.findAll();
    }

    @GetMapping("/bookings")
    public ResponseEntity<List<Booking>> getAllBookings(@RequestParam(required = false) String date) {
        try{
            List<Booking> bookings = new ArrayList<>();

            if (date == null){
                bookings.addAll(bookingRepository.findAll());
            } else {
                bookings.addAll(bookingRepository.findByDate(date));
            }
            if (bookings.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(bookings, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("ERROR --- could not get all bookings");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customerbookings/{id}")
    public ResponseEntity<List<Booking>> getBookingsByCustomer_Id(@PathVariable("id") long customer_id){
        try{
            List<Booking> bookings = new ArrayList<>();

            bookings.addAll(bookingRepository.findBookingsByCustomer_id(customer_id));

            if (bookings.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(bookings, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println("ERROR --- could not get all bookings");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/employeebookings/{id}")
    public ResponseEntity<List<Booking>> getBookingsByEmployee_Id(@PathVariable("id") long employee_id){
        try{
            List<Booking> bookings = new ArrayList<>();

            bookings.addAll(bookingRepository.findBookingsByEmployee_id(employee_id));

            if (bookings.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(bookings, HttpStatus.OK);


        }catch (Exception e) {
            System.out.println("ERROR --- could not get all bookings");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bookings/{id}")
    public ResponseEntity<Booking> getBookingsById(@PathVariable("id") long id){
        Optional<Booking> bookingData = bookingRepository.findById(id);

        if (bookingData.isPresent()) {
            return new ResponseEntity<>(bookingData.get(), HttpStatus.OK);
        } else {
            System.out.println("ERROR --- could not find booking by id: " + id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addbooking")
    public ResponseEntity<?> registerBooking(@Valid @RequestBody BookingRequest bookingRequest){
       try{
            Booking booking = new Booking(
                    bookingRequest.getCustomer_id(),
                    null,
                    bookingRequest.getCleaning_service_id(),
                    bookingRequest.getDate(),
                    bookingRequest.getTime(),
                    new Timestamp(new Date().getTime()),
                    EStatus.OBEKRÄFTAD,
                    null
            );

            bookingRepository.save(booking);
            return ResponseEntity.ok(
                    new MessageResponse("Booking complete and successfully registered"));


        } catch (Exception e) {
            System.out.println("ERROR --- could not send booking request");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/bookings/{id}")
    public ResponseEntity<Booking> updateBooking(@PathVariable("id") long id, @RequestBody Booking booking) {
        Optional<Booking> bookingData = bookingRepository.findById(id);

        if (bookingData.isPresent()){
            Booking _booking = bookingData.get();
            _booking.setCustomer_id(booking.getCustomer_id());
            _booking.setEmployee_id(booking.getEmployee_id());
            _booking.setCleaning_service_id(booking.getCleaning_service_id());
            _booking.setDate(booking.getDate());
            _booking.setTime(booking.getTime());
            _booking.setTimestamp(booking.getTimestamp());
            _booking.setStatus(booking.getStatus());
            _booking.setComment(booking.getComment());
            if (booking.getStatus().equals(EStatus.BEKRÄFTAD) && booking.getEmployee_id() != null) {
                //            SmsResponse.smsMain();
            }

            return new ResponseEntity<>(bookingRepository.save(_booking), HttpStatus.OK);
        } else {
            System.out.println("ERROR --- could not update booking with id: " + id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/bookings/{id}")
    public ResponseEntity<HttpStatus> deleteBooking(@PathVariable("id") long id) {
        try {
            bookingRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println("ERROR --- could not delete booking with id " + id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/bookings")
    public ResponseEntity<HttpStatus> deleteAllBookings() {
        try {
            bookingRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT); // NO_CONTENT
        } catch (Exception e) {
            System.out.println("ERROR --- could not delete all bookings");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bookings/customer-history")
    public ResponseEntity<List <Booking>> getBookingsByCustomerIdAndStatus(@RequestParam Long customer_id, @RequestParam EStatus status) {
        try {
            return new ResponseEntity<>(bookingRepository.findByCustomer_idAndStatus(customer_id, status), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/bookings/employee-history")
    public ResponseEntity<List <Booking>> getBookingsByEmployeeIdAndStatus(@RequestParam Long employee_id, @RequestParam EStatus status) {
        try {
            return new ResponseEntity<>(bookingRepository.findByEmployee_idAndStatus(employee_id, status), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bookings/admin-status")
    public ResponseEntity<List<Booking>> getBookingByStatus(@RequestParam EStatus status) {
        try {
            return new ResponseEntity<>(bookingRepository.findByStatus(status), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

