package com.t3.stadafint.controllers;

import com.t3.stadafint.entities.ERole;
import com.t3.stadafint.entities.Role;
import com.t3.stadafint.entities.User;
import com.t3.stadafint.repositories.RoleRepository;
import com.t3.stadafint.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/t3")
public class AdminController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/employees")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> fetchEmployees() {
        Optional<Role> roles = roleRepository.findByName(ERole.ROLE_EMPLOYEE);
        return userRepository.findUserByRoles(roles);
    }

    @GetMapping("/customers")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> fetchCustomers() {
        Optional<Role> roles = roleRepository.findByName(ERole.ROLE_CUSTOMER);
        return userRepository.findUserByRoles(roles);
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<User>> fetchAllUsers() {
        try {
            List<User> users = new ArrayList<>();
            users.addAll(userRepository.findAll());

            if (users.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("ERROR --- could not get all bookings");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<User> fetchUsersById(@PathVariable("id") long id) {
        Optional<User> customerData = userRepository.findById(id);

        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            System.out.println("ERROR --- could not find booking by id: " + id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User user) {
        Optional<User> userData = userRepository.findById(id);

        if (userData.isPresent()) {
            User _user = userData.get();
            _user.setEmail(user.getEmail());
            _user.setUsername(user.getUsername());
            _user.setFirstname(user.getFirstname());
            _user.setLastname(user.getLastname());
            _user.setPhone(user.getPhone());
            _user.setAddress(user.getAddress());
            _user.setPostalcode(user.getPostalcode());
            _user.setCity(user.getCity());

            return new ResponseEntity<>(userRepository.save(_user), HttpStatus.OK);
        } else {
            System.out.println("ERROR --- could not update booking with id: " + id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") long id) {
        try {
            userRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println("ERROR --- could not delete booking with id " + id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> deleteAllUsers() {
        try {
            userRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT); // NO_CONTENT
        } catch (Exception e) {
            System.out.println("ERROR --- could not delete all bookings");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
