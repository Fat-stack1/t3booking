import React, { useState, useEffect } from "react";
import {Switch, Route, Link, Redirect} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import AuthService from "./services/auth.service";

import NotFound from "./components/public/NotFound";

import Login from "./components/public/Login";
import RegisterUser from "./components/public/RegisterUser";
import Home from "./components/public/Home";

import BoardCustomer from "./components/BoardCustomer";
import BoardAdmin from "./components/BoardAdmin";
import BoardEmployee from "./components/BoardEmployee";
import logo from './resources/DammTussen256x256sponge.png';

import NavBar from "./components/public/NavBar";

const App = () => {
  const [currentUser, setCurrentUser] = useState(AuthService.getCurrentUser);
  const [role, setRole] = useState(undefined);

  useEffect(() => {
    if (currentUser) {
      handleRole();
    }

  }, [])

  const handleRole = () => {
    setCurrentUser(AuthService.getCurrentUser());
    if (currentUser.roles.includes("ROLE_ADMIN")) {
      setRole("admin");
    }
    if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
      setRole("employee")
    }
    if (currentUser.roles.includes("ROLE_CUSTOMER")) {
      setRole("customer")
    }
  }

  return (
      <div>
        {currentUser ? <NavBar role={role}/> : ( !currentUser &&
            <nav className="navbar navbar-expand navbar-dark bg-dark ">
              <button
                  className="btn btn-outline-dark"
                  data-toggle="tooltip"
                  data-placement={"bottom"}
                  title={"Visa lönespecifikation"}
                  onClick={"window.location.href='';"}>
                <img className="mx-4" style={{"width" : "64px", "height" : "64px"}} src={logo} alt="Dammtussen-logo"/>
              </button>

              <Link to={"/"} className="navbar-brand">
                <h1 className="my-4">StädaFint AB</h1>
              </Link>
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/login"} className="nav-link">
                    LOGGA IN
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to={"/register"} className="nav-link">
                    SKAPA KONTO
                  </Link>
                </li>
              </div>
            </nav>
        )}

        <div>
          <Switch>
            {!currentUser &&
                <>
                  <Route exact path={["/", "/home"]} component={Home} />
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/register" component={RegisterUser} />
                </>

            }
            {currentUser ? (
                <>
                  {role === "customer" ? (
                      <Route path="/:role" component={BoardCustomer} />
                  ) : <Redirect to={"/:role"}/> }
                  {role === "admin" ? (
                      <Route path="/:role" component={BoardAdmin} />
                  ) : <Redirect to={"/:role"}/> }
                  {role === "employee" ? (
                      <Route path="/:role" component={BoardEmployee} />
                  ) : <Redirect to={"/:role"}/> }
                </>
            ) : <Redirect to={"/"}/> }

            <Route path="/*" component={NotFound}/>
          </Switch>
        </div>
      </div>
  );
};

export default App;