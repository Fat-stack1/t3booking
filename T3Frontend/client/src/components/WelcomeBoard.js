import React from "react";
import AuthService from "../services/auth.service"
import CleaningCard from "./public/CleaningCard";
import esp5a from "../resources/esp-5-a.png";
import esp5b from "../resources/esp-5-b.png";
import esp572 from "../resources/esp-572.png"
import esp5d from "../resources/esp-5-d.png";
import esp571 from "../resources/esp-5.7.1.png";
import ImageCredits from "./public/ImageCredits";
import esp5 from "../resources/DammTussen256x256sponge.png"

const WelcomeBoard = () => {
    const user = AuthService.getCurrentUser();

    return (
        <>
            <div className={"d-flex flex-column mx-3 w-auto"}>
                <div className={"d-flex flex-column justify-content-center align-content-center text-center mx-5 w-auto"}>
                    <h3 className={"text-info d-flex justify-content-center text-center mx-5 mb-2 mt-5 w-auto"}>
                        <strong>Senaste nyheterna från Städa Fint AB </strong>
                    </h3>
                    <hr/>
                    <div className={"text-secondary d-flex flex-row justify-content-center mx-5 mb-5"}>
                        <h4 className={"border-end mx-3 justify-content-around bg-light"}>2022</h4>
                        <div className={"d-flex flex-column"}>
                            <ImageCredits/>
                        </div>
                    </div>
                    <div className={"d-flex flex-row justify-content-center mx-5 mb-5"}>
                        <CleaningCard/>
                    </div>
                    <div className={"text-secondary d-flex justify-content-center text-center mx-5 mb-5"}>
                        <h4 className={"border-end mx-3 justify-content-around bg-secondary bg-light"}>2021</h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Condimentum mattis pellentesque id nibh tortor id. Nunc eget lorem dolor sed. Pretium aenean pharetra magna ac placerat vestibulum. Quam adipiscing vitae proin sagittis nisl rhoncus. Varius quam quisque id diam vel quam elementum pulvinar. Donec ac odio tempor orci dapibus ultrices in iaculis. Facilisi cras fermentum odio eu feugiat pretium. Non quam lacus suspendisse faucibus interdum posuere lorem. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam. Risus nullam eget felis eget nunc lobortis mattis aliquam. Dui sapien eget mi proin sed libero enim sed.
                    </div>
                </div>
            </div>
        </>
    )
}
export default WelcomeBoard;