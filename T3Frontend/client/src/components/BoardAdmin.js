import React, { useState, useEffect } from "react";
import UserService from "../services/user.service";
import EventBus from "../common/EventBus";
import UserTable from "./admin/users/UserTable";
import BookingTable from "./booking/BookingTable";
import AuthService from "../services/auth.service";
import Economy from "./admin/Economy";
import Profile from "./Profile";
import RegisterBooking from "./booking/RegisterBooking";
import InvoiceTable from "./admin/invoices/InvoiceTable";
import RegisterUser from "./public/RegisterUser";
import SalaryTable from "./admin/salaries/SalaryTable";

// ICONS
import AccountBalanceIcon from '@material-ui/icons/AccountBalanceTwoTone';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircleTwoTone';
import FaceIcon from '@material-ui/icons/FaceTwoTone';
import PersonAddIcon from '@material-ui/icons/PersonAddTwoTone';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenterTwoTone';
import HomeIcon from '@material-ui/icons/HomeTwoTone';
import LibraryAddIcon from '@material-ui/icons/LibraryAddTwoTone';
import ReceiptIcon from '@material-ui/icons/ReceiptTwoTone';
import LocalAtmIcon from '@material-ui/icons/LocalAtmTwoTone';
import WelcomeBoard from "./WelcomeBoard";


const BoardAdmin = () => {
    const user = AuthService.getCurrentUser();

    const [content, setContent] = useState("");
    const [showProfile, setShowProfile] = useState(false);
    const [showWelcome, setShowWelcome] = useState(true);

    const [showUsers, setShowUsers] = useState(false);
    const [showAddUser, setShowAddUser] = useState(false);
    const [showUsersItems, setShowUsersItems] = useState(false);

    const [showBookings, setShowBookings] = useState(false);
    const [showAddBooking, setShowAddBooking] = useState(false);
    const [showBookingItems, setShowBookingItems] = useState(false);

    const [showEconomy, setShowEconomy] = useState(false);
    const [showInvoiceItems, setShowInvoiceItems] = useState(false);
    const [showInvoices, setShowInvoices] = useState(false);
    const [showSalary, setShowSalary] = useState(false);

    useEffect(() => {
        UserService.getAdminBoard().then(
            (response) => {
                setContent(response.data);
            },
            (error) => {
                const _content =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                setContent(_content);

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            }
        );
    }, []);

    const handleViewBookings = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(true);               // <-- MainNavView
        setShowBookingItems(!showBookingItems);    // <-- SubNav
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(false);
        setShowInvoices(false);
        setShowSalary(false);
    }
    const handleViewAddBookings = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(true);          // <-- SubNav
        setShowAddBooking(true);            // <-- View

        setShowEconomy(false);
        setShowInvoiceItems(false);
        setShowInvoices(false);
        setShowSalary(false);
    }

    const handleViewUsers = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(true);                  // <-- MainNavView
        setShowUsersItems(!showUsersItems);        // <-- SubNav
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(false);
        setShowInvoices(false);
        setShowSalary(false);
    }
    const handleViewAddUser = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(false);                 // <-- MainNavView
        setShowUsersItems(true);             // <-- SubNav
        setShowAddUser(true);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(false);
        setShowInvoices(false);
        setShowSalary(false);
    }

    const handleViewEconomy = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(true);               // <-- MainNavView
        setShowInvoiceItems(!showInvoiceItems);   // <-- SubNav
        setShowInvoices(false);
        setShowSalary(false);
    }

    const handleViewProfile = () => {
        setShowProfile(true);                // <-- MainNavView
        setShowWelcome(false);

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(false);
        setShowInvoices(false);
        setShowSalary(false);
    }
    const handleViewWelcome = () => {
        setShowProfile(false);
        setShowWelcome(true);               // <-- MainNavView

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(false);
        setShowInvoices(false);
        setShowSalary(false);
    }

    const handleViewInvoice = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(true);           // <-- SubNav
        setShowInvoices(true);               // <-- View
        setShowSalary(false);
    }

    const handleViewSalary = () => {
        setShowProfile(false);
        setShowWelcome(false);

        setShowUsers(false);
        setShowUsersItems(false);
        setShowAddUser(false);

        setShowBookings(false);
        setShowBookingItems(false);
        setShowAddBooking(false);

        setShowEconomy(false);
        setShowInvoiceItems(true);          // <-- SubNav
        setShowInvoices(false);
        setShowSalary(true);                // <-- View
    }

    return (
        <div className="d-flex flex-row vh-100" >
            <div className=" border border-left-0 border-top-0 border-bottom-0 d-flex" style={{"width":"255px"}}>
                <div className=" bg-light " style={{"width":"251px"}}>
                    <nav className="navbar navbar-expand-lg navbar-light d-flex" style={{"width":"250px"}}>
                        <div className="" id="navbarNav" style={{"width":"251px"}}>
                            <ul className="navbar-nav d-flex flex-column">
                                <li className="nav-item active d-flex border-bottom">
                                    <span className="nav-link d-flex" onClick={handleViewWelcome} style={{cursor: "pointer"}}><div className={"mx-2"}><HomeIcon/> <strong>{content} {user.firstname}</strong></div></span>
                                </li>
                                <li className="nav-item active d-flex border-bottom">
                                    <span className="nav-link d-flex" onClick={handleViewProfile} style={{cursor: "pointer"}}><div className={"mx-2"}><FaceIcon/> <i>{user.username}</i></div></span>
                                </li>
                                <li className="nav-item d-flex border-bottom">
                                    <span className="nav-link d-flex" onClick={handleViewUsers} style={{cursor: "pointer"}}><div className={"mx-2"}><SupervisedUserCircleIcon/> Användare</div></span>
                                </li>
                                {showUsersItems ?
                                    <ul>
                                        <li className="nav-item d-flex">
                                            <span className="nav-link d-flex" onClick={handleViewAddUser} style={{cursor: "pointer"}}><div className={"mx-2"}><PersonAddIcon/> <i>Ny användare</i></div></span>
                                        </li>
                                    </ul> : <></>
                                }
                                <li className="nav-item d-flex border-bottom">
                                        <span className="nav-link d-flex" onClick={handleViewBookings} style={{cursor: "pointer"}}><div className={"mx-2"}><BusinessCenterIcon/> Bokningar</div></span>
                                </li>
                                {showBookingItems ?
                                    <ul>
                                        <li className="nav-item d-flex">
                                            <span className="nav-link d-flex" onClick={handleViewAddBookings} style={{cursor: "pointer"}}><div className={"mx-2"}><LibraryAddIcon/> <i>Ny bokning</i></div></span>
                                        </li>
                                    </ul> : <></>
                                }
                                <li className="nav-item d-flex border-bottom">
                                    <span className="nav-link d-flex" onClick={handleViewEconomy} style={{cursor: "pointer"}}><div className={"mx-2"}><AccountBalanceIcon/> Ekonomi</div></span>
                                </li>
                                {showInvoiceItems ?
                                    <ul>
                                        <li className="nav-item d-flex">
                                            <span className="nav-link d-flex" onClick={handleViewInvoice} style={{cursor: "pointer"}}><div className={"mx-2"}><ReceiptIcon/> <i>Fakturor</i></div></span>
                                        </li>
                                        <li className="nav-item d-flex">
                                            <span className="nav-link d-flex" onClick={handleViewSalary} style={{cursor: "pointer"}}><div className={"mx-2"}><LocalAtmIcon/> <i>Löner</i></div></span>
                                        </li>
                                    </ul> : <></>
                                }
                                </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div className="w-100 d-flex justify-content-center">
                {!showWelcome ? (<></>) : (showWelcome && <WelcomeBoard/>)}
                {!showProfile ? (<></>) : (showProfile && <Profile/>)}

                {!showUsers ? ( <></> ) : (showUsers && <UserTable/> )}
                {!showAddUser ? ( <></> ) : (showAddUser && <div className="h-100 w-100"><RegisterUser/></div> )}
                {!showBookings ? ( <></> ) : (showBookings && <BookingTable/>)}
                {!showAddBooking ? ( <></> ) : (showAddBooking && <RegisterBooking/>) }

                {!showEconomy ? ( <></> ) : (showEconomy && <Economy/>) }
                {!showInvoices ? ( <></> ) : (showInvoices && <InvoiceTable/>) }
                {!showSalary ? ( <></> ) : (showSalary && <SalaryTable/>) }
            </div>
        </div>
    );
}
export default BoardAdmin;