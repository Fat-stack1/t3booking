import React, { useState } from "react";
import BookingRow from "../booking/BookingRow";
import Modal from "react-bootstrap/Modal";
import EditBooking from "../booking/EditBooking";
import SalaryTemplate from "./salaries/SalaryTemplate";


const Salary = (props) => {

    const [showModalSalary, setShowModalSalary] = useState(false);


    const handleCloseModalSalary = () => {
        setShowModalSalary(false);
    }
    const handleShowModalSalary = () => setShowModalSalary(true);


    let salaries = [
        {
            "employee_id": "1",
            "date": "2021-12-25",
            "hours": "40",
            "gross": "29 000",
            "net": "20300",
        },
    ]

    return (
        <>
            <div className="d-flex flex-column mx-5 my-5 w-100">
                <h2 >Lönespecifikationer</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <th className="text-center">Anställnings-ID</th>
                            <th className="text-center">Datum</th>
                            <th className="text-center">Timmar</th>
                            <th className="text-center">Brutto (kr)</th>
                            <th className="text-center">Netto (kr)</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="text-center">{salaries[0].employee_id}</td>
                            <td className="text-center">{salaries[0].date}</td>
                            <td className="text-center">{salaries[0].hours}</td>
                            <td className="text-center">{salaries[0].gross}</td>
                            <td className="text-center">{salaries[0].net}</td>
                            <td className="text-center"><button className="btn btn-dark" onClick={handleShowModalSalary}>Detaljer</button></td>
                            <>
                                <Modal show={showModalSalary} onHide={handleCloseModalSalary}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Lönespecifikation - Detaljer</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div className="d-flex flex-row justify-content-center">
                                            <SalaryTemplate />
                                        </div>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <button className="btn btn-dark" onClick={handleCloseModalSalary}>Stäng</button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}
export default Salary;