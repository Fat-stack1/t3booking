import React, {useEffect, useState} from "react";
import UserService from "../../../services/user.service";
import Modal from "react-bootstrap/Modal";
import InvoiceTemplate from "./InvoiceTemplate";
import BookingService from "../../../services/booking.service";
import dayjs from "dayjs";
import InfoIcon from '@material-ui/icons/InfoTwoTone';

const InvoiceRow = (props) => {
    const { id,
        customer_id,
        cleaning_service_id,
        date,
        status,
    } = props;

    const initialInvoiceState =
        {
            id: id,
            customer_id: customer_id,
            cleaning_service_id: cleaning_service_id,
            date: date,
            dueDate: "2021-12-15",
            status: status,
            ocr: "89074258436253",
            bankGiro: "123-4567",
            price: null
        };

    const [invoice, setInvoice] = useState(initialInvoiceState)
    const [showModalInvoice, setShowModalInvoice] = useState(false);
    const [price, setPrice] = useState(null);

    useEffect(() => {
        getInvoice(id);
    }, [id]);

    const handleCloseModalInvoice = () => {
        setShowModalInvoice(false);
    }
    const handleShowModalInvoice = () => setShowModalInvoice(true);

    const getPrices = () => {
        UserService.getCleaningServices().then(response => {
            setPrice(response.data);
        })
            .catch(e => {
            console.log(e);
        });
    }
    const getInvoice = (id) => {
        BookingService.getBookingById(id)
            .then(response => {
                setInvoice(response.data);
                console.log(response.data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    return(
        <>
            <td className="text-center">{invoice.id}</td>
            <td className="text-center">{invoice.customer_id}</td>
            <td className="text-center">{invoice.cleaning_service_id}</td>
            <td>{dayjs(invoice.date).format('YYYY-MM-DD')}</td>
            <td className="text-center">{initialInvoiceState.dueDate}</td>
            <td className="text-center">{invoice.status}</td>
            <td className="text-center">{initialInvoiceState.ocr}</td>
            <td className="text-center">
                <button
                    className="btn btn-outline-dark"
                    data-toggle="tooltip"
                    data-placement={"bottom"}
                    title={"Visa faktura"}
                    onClick={handleShowModalInvoice}>
                    <InfoIcon/>
                </button>
            </td>
            <>
                <Modal show={showModalInvoice} onHide={handleCloseModalInvoice}>
                    <Modal.Header closeButton>
                        <Modal.Title>Faktura - Detaljer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex flex-row justify-content-center">
                            <InvoiceTemplate

                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalInvoice}>Stäng</button>
                    </Modal.Footer>
                </Modal>
            </>
        </>
    )
}
export default InvoiceRow;