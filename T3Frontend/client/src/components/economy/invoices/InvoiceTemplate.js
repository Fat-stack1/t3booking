import React, { useState } from "react";
import logo from "../../../resources/DammTussen256x256sponge.png";
import BookingRow from "../../booking/BookingRow";

const InvoiceTemplate = props => {

    return (
        <>
            <div className="d-flex flex-column border mt-3" style={{ "width": "600px" }}>

                <div className="d-flex flex-row justify-content-between mx-3">
                    <div className="d-flex mt-2"><h1 className="text-info d-flex">Faktura</h1></div>
                    <img className="mx-4 d-flex" style={{ "width": "64px", "height": "64px" }} src={logo}
                        alt="Dammtussen-logo" />
                </div>

                <hr className="bg-info mx-3" style={{ "height": "3px" }} />

                <div className="d-flex flex-column mx-3 mb-4">
                    <h4 className="d-flex my-0">StädaFint AB</h4>
                    <p className="d-flex my-0">Malmövägen 1</p>
                    <p className="d-flex my-0">245 25 Malmö, Sverige</p>
                    <p className="d-flex my-0">Telefon: +46-12131415</p>
                </div>

                <div className="d-flex flex-column mx-2 bg-light px-2 py-2">

                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "35px" }}>Fakturadatum:</p>
                        <p className="d-flex my-0">2021-12-01</p>
                    </div>
                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "30px" }}>Fakturnummer:</p>
                        <p className="d-flex my-0">2346231</p>
                    </div>
                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "37px" }}>Kundnummer:</p>
                        <p className="d-flex my-0">1</p>
                    </div>
                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "53px" }}>Förfallodag:</p>
                        <p className="d-flex my-0">2021-12-01</p>
                    </div>
                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "17px" }}>Betalningsvillkor:</p>
                        <p className="d-flex my-0">14 dagar</p>
                    </div>
                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "70px" }}>Bankgiro:</p>
                        <p className="d-flex my-0">123-4567</p>
                    </div>
                    <div className="d-flex flex-row my-0">
                        <p className="d-flex my-0" style={{ "margin-right": "102px" }}>OCR:</p>
                        <p className="d-flex my-0">6583673556475</p>
                    </div>
                </div>

                <hr className="bg-info mx-3" style={{ "height": "3px" }} />

                <div className="d-flex flex-column mx-2 px-2">
                    <table className="table text-center table-striped">
                        <thead>
                            <tr>
                                <th><h6>Beskrivning</h6></th>
                                <th><h6>Antal h:</h6></th>
                                <th><h6>à pris</h6></th>
                                <th><h6>Moms</h6></th>
                                <th><h6>Moms kr</h6></th>
                                <th><h6>Belopp</h6></th>
                            </tr>
                        </thead>
                        <tbody>
                            <td>Städtjänst här!</td>
                            <td>3</td>
                            <td>249</td>
                            <td>20%</td>
                            <td>149</td>
                            <td>896</td>
                        </tbody>
                    </table>
                </div>

                <div className="d-flex flex-column mx-2 px-2 py-2">
                    <div className="d-flex flex-row my-0 justify-content-end">
                        <h6 className="d-flex my-0" >Belopp före moms</h6>
                        <p className="d-flex my-0" style={{ "margin-left": "30px" }}>747 kr</p>
                    </div>
                    <div className="d-flex flex-row my-0 justify-content-end">
                        <h6 className="d-flex my-0" >Total moms</h6>
                        <p className="d-flex my-0" style={{ "margin-left": "30px", "margin-right": "0px" }}>149 kr</p>
                    </div>
                    <div className="d-flex flex-row my-0 justify-content-end">
                        <h6 className="d-flex my-0 text-info">Summa att betala</h6>
                        <p className="d-flex my-0 text-info" style={{ "margin-left": "30px", "margin-right": "0px" }}>896 kr</p>
                    </div>
                </div>

                <hr className="bg-info mx-3" style={{ "height": "3px" }} />

                <div class="d-flex flex-row justify-content-evenly">
                    <div className="d-flex flex-column mx-3 mb-4">
                        <h6 className="d-flex my-0">StädaFint AB</h6>
                        <p className="d-flex my-0">Malmövägen 1</p>
                        <p className="d-flex my-0">245 25 Malmö, Sverige</p>
                        <p className="d-flex my-0">Telefon: +46-12131415</p>
                    </div>
                    <div className="d-flex flex-column mx-3 mb-4">
                        <h6 className="d-flex my-0">Kontaktuppgifter</h6>
                        <p className="d-flex my-0">Anställd namn</p>
                        <p className="d-flex my-0">Telefon: +46-12131415</p>
                        <p className="d-flex my-0">Epost: anstalld@stadafint.se</p>
                    </div>
                </div>
                <span className="d-flex bg-info mt-2" style={{ "height": "40px" }}></span>
            </div>
        </>
    )
}
export default InvoiceTemplate;