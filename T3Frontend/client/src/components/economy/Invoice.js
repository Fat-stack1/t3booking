import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import InvoiceTemplate from "./invoices/InvoiceTemplate";

const Invoice = (props) => {

    const [showModalInvoice, setShowModalInvoice] = useState(false);


    const handleCloseModalInvoice = () => {
        setShowModalInvoice(false);
    }
    const handleShowModalInvoice = () => setShowModalInvoice(true);


    let invoices = [
        {
            "id": "1",
            "customer_id": "1",
            "cleaning_service_id": "1",
            "date": "2021-12-01",
            "OCR": "89074258436253",
            "bankNumber": "123-4567",
            "dateOfPayment": "2021-12-15"
        },
    ]

    return (
        <>
            <div className="d-flex flex-column my-5 w-100">
                <h2 >FAKTUROR</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <th className="text-center">Faktura-ID</th>
                            <th className="text-center">Kund-ID</th>
                            <th className="text-center">Städtjänst-ID</th>
                            <th className="text-center">Datum</th>
                            <th className="text-center">OCR</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="text-center">{invoices[0].id}</td>
                            <td className="text-center">{invoices[0].customer_id}</td>
                            <td className="text-center">{invoices[0].cleaning_service_id}</td>
                            <td className="text-center">{invoices[0].date}</td>
                            <td className="text-center">{invoices[0].OCR}</td>
                            <td className="text-center"><button className="btn btn-dark" onClick={handleShowModalInvoice}>Detaljer</button></td>
                            <>

                                <Modal show={showModalInvoice} onHide={handleCloseModalInvoice}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Faktura - Detaljer</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div className="d-flex flex-row justify-content-center">
                                            <InvoiceTemplate />
                                        </div>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <button className="btn btn-dark" onClick={handleCloseModalInvoice}>Stäng</button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}
export default Invoice;