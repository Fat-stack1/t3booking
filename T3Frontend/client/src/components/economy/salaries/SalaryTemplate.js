import React, { useState } from "react";
import logo from "../../../resources/DammTussenDammig.ico";
import BookingRow from "../../booking/BookingRow";

const SalaryTemplate = props => {

    return (
        <>
            <div className="d-flex flex-column border mt-3" style={{ "width": "600px" }}>

                <div className="d-flex flex-row justify-content-between mx-3">
                    <div className="d-flex flex-column justify-content-between mx-3">
                        <div className="d-flex mt-2"><h1 className="text-secondary d-flex">Lönespecifikation</h1></div>
                        <div className="d-flex mt-2"><h3 className="text-dark d-flex">December 2021</h3></div>
                    </div>
                    <img className="mx-4 d-flex my-4" style={{ "width": "64px", "height": "64px" }} src={logo}
                        alt="Dammtussen-logo" />
                </div>

                <hr className="bg-secondary mx-3" style={{ "height": "3px" }} />

                <div className="d-flex flex-column mx-3 mb-4">
                    <h5 className="d-flex my-0">Anställd Anställdsson</h5>
                    <p className="d-flex my-0">Personnummer: 901224-2412</p>
                </div>


                <hr className="bg-secondary mx-3" style={{ "height": "3px" }} />

                <div className="d-flex flex-column mx-2 px-2">
                    <table className="table text-center table-striped">
                        <thead>
                            <tr>
                                <th><h6>Anställnings-ID</h6></th>
                                <th><h6>Bankkonto</h6></th>
                                <th><h6>Skattetabell</h6></th>
                                <th><h6>Löneperiod</h6></th>
                            </tr>
                        </thead>
                        <tbody>
                            <td>2</td>
                            <td>8523-2124903</td>
                            <td>34:1</td>
                            <td>2021-12-01 till 2021-12-28</td>
                        </tbody>
                    </table>
                </div>

                <div className="d-flex flex-column mx-2 px-2 py-2">
                    <div className="d-flex flex-row my-0 justify-content-end">
                        <h6 className="d-flex my-0" >Bruttolön</h6>
                        <p className="d-flex my-0" style={{ "margin-left": "30px" }}>29 000 kr</p>
                    </div>
                    <div className="d-flex flex-row my-0 justify-content-end">
                        <h6 className="d-flex my-0" >Skatt (kr)</h6>
                        <p className="d-flex my-0" style={{ "margin-left": "30px", "margin-right": "13px" }}>8700 kr</p>
                    </div>
                    <div className="d-flex flex-row my-0 justify-content-end">
                        <h6 className="d-flex my-0 text-secondary">Nettolön</h6>
                        <p className="d-flex my-0 text-secondary" style={{ "margin-left": "30px", "margin-right": "0px" }}>20 300 kr</p>
                    </div>
                </div>

                <hr className="bg-secondary mx-3" style={{ "height": "3px" }} />

                <div className="d-flex flex-row justify-content-start">
                    <div className="d-flex flex-column mx-3 mb-4">
                        <h6 className="d-flex my-0">StädaFint AB</h6>
                        <p className="d-flex my-0">Malmövägen 1</p>
                        <p className="d-flex my-0">245 25 Malmö, Sverige</p>
                        <p className="d-flex my-0">Telefon: +46-12131415</p>
                    </div>
                </div>
                <span className="d-flex bg-secondary mt-2" style={{ "height": "40px" }} />
            </div>
        </>
    )
}
export default SalaryTemplate;