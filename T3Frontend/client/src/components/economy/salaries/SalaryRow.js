import React, {useEffect, useState} from "react";
import UserService from "../../../services/user.service";
import Modal from "react-bootstrap/Modal";
import BookingService from "../../../services/booking.service";
import SalaryTemplate from "./SalaryTemplate";
import InfoIcon from "@material-ui/icons/InfoTwoTone";

const SalaryRow = (props) => {
    const { id,
        employee_id,
        cleaning_service_id,
        status,
    } = props;

    const initialSalaryState =
        {
            id: id,
            employee_id: employee_id,
            cleaning_service_id: cleaning_service_id,
            date: "2021-12-25",
            hours: 40,
            status: status,
            gross: "29 000",
            net: "20 300",
            tax_table: "SKV 403"
        };

    const [salary, setSalary] = useState(initialSalaryState)
    const [showModalSalary, setShowModalSalary] = useState(false);
    const [amount, setAmount] = useState(null);
    const [employee, setEmployee] = useState([]);

    useEffect(() => {
        getSalary(id);
    }, [id]);

    const handleCloseModalSalary = () => {
        setShowModalSalary(false);
    }
    const handleShowModalSalary = () => setShowModalSalary(true);

    const getAmount = () => {
        UserService.getCleaningServices().then(response => {
            setAmount(response.data);
        })
            .catch(e => {
                console.log(e);
            });
    };

    const getSalary = (id) => {
        BookingService.getBookingById(id)
            .then(response => {
                setSalary(response.data);
                console.log(response.data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    // const getEmployee = () => {
    //     AuthService.getCurrentUser().then(response => {
    //         setEmployee(response.data);
    //     })
    //     .catch(e => {
    //         console.log(e);
    //     })
    // };

    return(
        <>
            <td className="text-center">{salary.id}</td>
            <td className="text-center">{salary.employee_id}</td>
            <td className="text-center">{salary.cleaning_service_id}</td>
            <td className="text-center">{initialSalaryState.date}</td>
            <td className="text-center">{initialSalaryState.hours}</td>
            <td className="text-center">{salary.status}</td>
            <td className="text-center">{initialSalaryState.gross}</td>
            <td className="text-center">{initialSalaryState.net}</td>
            <td className="text-center">{initialSalaryState.tax_table}</td>
            <td className="text-center">
                <button
                    className="btn btn-outline-dark"
                    data-toggle="tooltip"
                    data-placement={"bottom"}
                    title={"Visa lönespecifikation"}
                    onClick={handleShowModalSalary}>
                    <InfoIcon/>
                </button>
            </td>
            <>
                <Modal show={showModalSalary} onHide={handleCloseModalSalary}>
                    <Modal.Header closeButton>
                        <Modal.Title>Lönespecifikation - Detaljer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex flex-row justify-content-center">
                            <SalaryTemplate
                                employee_id={employee_id}

                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalSalary}>Stäng</button>
                    </Modal.Footer>
                </Modal>
            </>
        </>
    )
}
export default SalaryRow;