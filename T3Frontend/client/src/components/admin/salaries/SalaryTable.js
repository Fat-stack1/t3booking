import React, { useState, useEffect, useRef } from "react";
import BookingService from "../../../services/booking.service";
import SalaryRow from "./SalaryRow";

const SalaryTable = () => {
    const [salaryRow, setSalaryRow] = useState([]);
    const [message, setMessage] = useState("");

    const [showSalaryTable, setShowSalaryTable] = useState(true);

    const salaryRef = useRef();
    salaryRef.current = salaryRow;

    useEffect(() => {
        // retrievePendingSalaries();
        setMessage("Klicka på kategorierna för att visa valda lönespecifikationer")
    }, []);

    const retrievePendingSalaries = () => {
        BookingService.getAdminBookingByStatus("UNDER_UTFÖRANDE")
            .then((response) => {
                setSalaryRow(response.data);
                handleShowTable();
            }).catch((e) => {
            console.log(e);
        });
    };

    const retrieveSalaries = () => {
        BookingService.getAdminBookingByStatus("UTFÖRT")
            .then((response) => {
                setSalaryRow(response.data);
                handleShowTable();
            }).catch((e) => {
            console.log(e);
        });
    };

    const retrievePayed = () => {
        BookingService.getAdminBookingByStatus("BETALD")
            .then((response) => {
                setSalaryRow(response.data);
                handleShowTable();
            }).catch((e) => {
            console.log(e);
        });
    };

    const handleShowTable = () => {
        setShowSalaryTable(true);
    };

    return (
        <div className="d-flex flex-row w-100 justify-content-center" >
            <div className="d-flex flex-column w-100">
                <div className="d-flex flex-column ">
                    <div className="d-flex flex-row justify-content-left mt-5 mb-2 mx-5 w-100">
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-secondary" onClick={retrievePendingSalaries}>Oregistrerade</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveSalaries}>Registrerade</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrievePayed}>Utbetalda</button>
                        </div>
                    </div>
                    <span className="d-flex flex-row justify-content-left mt-2 mb-2 mx-5 w-100 text-secondary">
                        <i>
                            {message}
                        </i>
                    </span>
                </div>
                <div className="d-flex flex-column mx-5">
                    <div className="d-flex flex-row justify-content-center">
                        {showSalaryTable &&
                        <table className="table text-center table-striped">
                            <thead>
                            <tr>
                                <th>Lönespec-ID</th>
                                <th>Anställnings-ID</th>
                                <th>Städtjänst-ID</th>
                                <th>Datum</th>
                                <th>Timmar</th>
                                <th>Status</th>
                                <th>Brutto (kr)</th>
                                <th>Netto (kr)</th>
                                <th>Skattetabell</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {salaryRow ? (salaryRow.map(salary =>
                                <tr key={salary.id}>
                                    <SalaryRow
                                        id={salary.id}
                                        customer_id={salary.customer_id}
                                        cleaning_service_id={salary.cleaning_service_id}
                                        date={salary.date}
                                        time={salary.time}
                                        status={salary.status}
                                    />
                                </tr>
                            )) : (
                                <tr>
                                    <h6>DET FINNS INGA FAKTUROR I SYSTEMET</h6>
                                </tr>
                            )}
                            </tbody>
                            <tfoot>
                            Städa Fint AB
                            </tfoot>
                        </table>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};
export default SalaryTable;
