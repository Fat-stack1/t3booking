import React, { useState, useEffect, useRef } from "react";
import BookingService from "../../../services/booking.service";
import InvoiceRow from "./InvoiceRow";

const InvoiceTable = () => {
    const [invoiceRow, setInvoiceRow] = useState([]);
    const [message, setMessage] = useState("");

    const [showInvoiceTable, setShowInvoiceTable] = useState(true);

    const bookingsRef = useRef();
    bookingsRef.current = invoiceRow;

    useEffect(() => {
        // retrievePendingInvoices();
        setMessage("Klicka på kategorierna för att visa valda fakturor")
    }, []);

    const retrievePendingInvoices = () => {
        BookingService.getAdminBookingByStatus("UTFÖRT")
            .then((response) => {
                setInvoiceRow(response.data);
                handleShowTable();
            }).catch((e) => {
            console.log(e);
        });
    };

    const retrieveInvoices = () => {
        BookingService.getAdminBookingByStatus("FAKTURERAD")
            .then((response) => {
                setInvoiceRow(response.data);
                handleShowTable();
            }).catch((e) => {
            console.log(e);
        });
    };

    const retrievePayed = () => {
        BookingService.getAdminBookingByStatus("BETALD")
            .then((response) => {
                setInvoiceRow(response.data);
                handleShowTable();
            }).catch((e) => {
            console.log(e);
        });
    };

    const removeAllInvoices = () => {
        BookingService.removeAll()
            .then((response) => {
                console.log(response.data);
                refreshList();
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const refreshList = () => {
        retrievePendingInvoices();
    };

    const handleShowTable = () => {
        setShowInvoiceTable(true);
    };

    return (
        <div className="d-flex flex-row w-100 justify-content-center" >
            <div className="d-flex flex-column w-100">
                <div className="d-flex flex-column ">
                    <div className="d-flex flex-row justify-content-left mt-5 mb-2 mx-5 w-100">
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-secondary" onClick={retrievePendingInvoices}>Ofakturerade</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn-danger" onClick={retrieveInvoices}>Obetalda</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrievePayed}>Betalda</button>
                        </div>
                    </div>
                    <span className="d-flex flex-row justify-content-left mt-2 mb-2 mx-5 w-100 text-secondary">
                        <i>
                            {message}
                        </i>
                    </span>
                </div>
                <div className="d-flex flex-column mx-5">
                    <div className="d-flex flex-row justify-content-center">
                        {showInvoiceTable &&
                        <table className="table text-center table-striped">
                            <thead>
                            <tr>
                                <th>Faktura-ID</th>
                                <th>Kund-ID</th>
                                <th>Städtjänst-ID</th>
                                <th>Datum</th>
                                <th>Förfallodag</th>
                                <th>Status</th>
                                <th>OCR</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {invoiceRow ? (invoiceRow.map(invoice =>
                                <tr key={invoice.id}>
                                    <InvoiceRow
                                        id={invoice.id}
                                        customer_id={invoice.customer_id}
                                        cleaning_service_id={invoice.cleaning_service_id}
                                        date={invoice.date}
                                        time={invoice.time}
                                        status={invoice.status}
                                    />
                                </tr>
                            )) : (
                                <tr>
                                    <h6>DET FINNS INGA FAKTUROR I SYSTEMET</h6>
                                </tr>
                            )}
                            </tbody>
                            <tfoot>
                            Städa Fint AB
                            </tfoot>
                        </table>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};
export default InvoiceTable;
