import React, { useEffect, useState } from "react";
import InvoiceTemplate from "../economy/invoices/InvoiceTemplate";
import SalaryTemplate from "../economy/salaries/SalaryTemplate";
import darkLogo from "../../resources/DammTussenDammig.ico";
import blueLogo from "../../resources/DammTussen256x256sponge.png";
import Modal from "react-bootstrap/Modal";
import InfoIcon from "@material-ui/icons/InfoTwoTone";
import BookingService from "../../services/booking.service";
import UserService from "../../services/user.service";

const Economy = (props) => {
    const { salaryId,
        employee_id,
        cleaning_service_id,
        status,
    } = props;

    const initialSalaryState =
    {
        id: salaryId,
        employee_id: employee_id,
        cleaning_service_id: cleaning_service_id,
        date: "2021-12-25",
        hours: 40,
        status: status,
        gross: "29 000",
        net: "20 300",
        tax_table: "SKV 403"
    };

    const { invoiceId,
        customer_id,
        cleaning_service_id2,
        date,
        status2,
    } = props;

    const initialInvoiceState =
    {
        id: invoiceId,
        customer_id: customer_id,
        cleaning_service_id: cleaning_service_id2,
        date: date,
        dueDate: "2021-12-15",
        status: status2,
        ocr: "89074258436253",
        bankGiro: "123-4567",
        price: null
    };

    const [salary, setSalary] = useState(initialSalaryState)
    const [invoice, setInvoice] = useState(initialInvoiceState)
    const [showModalSalary, setShowModalSalary] = useState(false);
    const [showModalInvoice, setShowModalInvoice] = useState(false);


    const handleCloseModalSalary = () => {
        setShowModalSalary(false);
    }
    const handleShowModalSalary = () => setShowModalSalary(true);

    const handleCloseModalInvoice = () => {
        setShowModalInvoice(false);
    }
    const handleShowModalInvoice = () => setShowModalInvoice(true);

    const getSalary = (salaryId) => {
        BookingService.getBookingById(salaryId)
            .then(response => {
                setSalary(response.data);
                console.log(response.data)
            })
            .catch(e => {
                console.log(e);
            });
    };



    const getInvoice = (invoiceId) => {
        BookingService.getBookingById(invoiceId)
            .then(response => {
                setInvoice(response.data);
                console.log(response.data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    // useEffect(() => {
    //     getSalary(salaryId);
    //     getInvoice(invoiceId);
    // }, [salaryId],[invoiceId]);

    return (
        <>
            <div className="d-flex flex-column mx-5 w-100 my-5">
                <div className="d-flex flex-column text-center w-100  mt-3">
                    <span className="mb-5">
                        <h3 className={"text-secondary mx-5 mt-3"}>
                            Navigera till faktura/lön för att hantera inkomster/utgifter via menyn till vänster.
                        </h3>
                    </span>

                    <section className={"d-flex flex-row justify-content-evenly"}>
                        <div className="class-card d-flex flex-column flex-wrap justify-content-center">
                            <div className={"d-flex flex-row border rounded border-dark"} style={{ "width": "500px" }}>
                                <div className="d-flex justify-content-center align-items-center border "
                                    style={{ "width": "200px" }}>
                                    <img className="d-flex mx-2" style={{ "width": "128px", "height": "128px" }}
                                        src={blueLogo} alt="Dammtussen-logo" />
                                </div>
                                <div
                                    className="d-flex flex-column text-white bg-info p-4 border align-items-center">
                                    <div className="display-8 heading-font mb-3"><h4>Fakturamall</h4></div>
                                    <div className="mb-4">Här kan du se mallen för kundfakturor
                                    </div>
                                    <button
                                        className="btn btn-info border btn-lg btn-warning text-uppercase border-dark my-1 w-100"
                                        data-toggle="tooltip"
                                        data-placement={"bottom"}
                                        title={"Visa faktura"}
                                        onClick={handleShowModalInvoice}>Visa mall

                                    </button>
                                    <div className="d-flex flex-row text-center">
                                        <p className="d-flex text-light ">Senast uppdaterad: 2022-01-20</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="class-card d-flex flex-column flex-wrap justify-content-center">
                            <div className={"d-flex flex-row border rounded border-dark"} style={{ "width": "500px" }}>
                                <div className="d-flex justify-content-center align-items-center border "
                                    style={{ "width": "250px" }}>
                                    <img className="d-flex mx-2" style={{ "width": "128px", "height": "128px" }}
                                        src={darkLogo} alt="Dammtussen-logo" />
                                </div>
                                <div
                                    className="d-flex flex-column text-white bg-dark p-4 border align-items-center">
                                    <div className="display-8 heading-font mb-3"><h4>Lönespecifikationmall</h4></div>
                                    <div className="mb-4">Här kan du se mallen för lönespecifikationer
                                    </div>
                                    <button
                                        className="btn btn-info border border-dark btn-lg btn-warning text-uppercase border-white my-1 w-100"
                                        data-toggle="tooltip"
                                        data-placement={"bottom"}
                                        title={"Visa faktura"}
                                        onClick={handleShowModalSalary}>Visa mall
                                    </button>
                                    <div className="d-flex flex-row text-center">
                                        <p className="d-flex text-light ">Senast uppdaterad: 2022-01-20</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



                    {/*
                    <span className={"d-flex flex-row justify-content-evenly"}>
                        <InvoiceTemplate/>
                        <SalaryTemplate/>
                    </span>
                    <span>

                    </span>*/}
                </div>
            </div>

            <Modal show={showModalInvoice} onHide={handleCloseModalInvoice}>
                <Modal.Header closeButton>
                    <Modal.Title><InfoIcon /> Faktura</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="d-flex flex-row justify-content-center">
                        <InvoiceTemplate />
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button className="btn btn-dark"
                        onClick={handleCloseModalInvoice}>Stäng
                    </button>
                </Modal.Footer>
            </Modal>

            <>
                <Modal show={showModalSalary} onHide={handleCloseModalSalary}>
                    <Modal.Header closeButton>
                        <Modal.Title>Lönespecifikation - Detaljer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex flex-row justify-content-center">
                            <SalaryTemplate />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalSalary}>Stäng</button>
                    </Modal.Footer>
                </Modal>
            </>

        </>
    )
}
export default Economy;