import React, {useState, useEffect, useRef} from "react";
import AdminService from "../../../services/admin.service";
import EditUser from "./EditUser";
import UserRow from "./UserRow";

const UserTable = () => {
    const [users, setUsers] = useState([]);
    const [showUserInfo, setShowUserInfo] = useState(false);
    const [showUserTable, setShowUserTable] = useState(false);
    const [message, setMessage] = useState("");

    const usersRef = useRef();
    usersRef.current = users;

    useEffect(() => {
        // retrieveUsersList();
        setMessage("Klicka på kategorierna för att sortera användarna")
    }, []);

    // TODO: Admins
    const retrieveUsersList = () => {
        AdminService.getAllUsers().then((response) => {
            setUsers(response.data);
            handleShowTable();
        }).catch((e) => {
            console.log(e);
        });
    };
    const retrieveCustomers = () => {
        AdminService.getCustomersList().then((response) => {
            setUsers(response.data);
            setShowUserInfo(false);
            setShowUserTable(true);
        }).catch((e) => {
            console.log(e);
        });
    };
    const retrieveEmployees = () => {
        AdminService.getEmployeesList().then((response) => {
            setUsers(response.data);
            setShowUserInfo(false);
            setShowUserTable(true);
        }).catch((e) => {
            console.log(e);
        });
    };

    const handleShowUser = () => {
        setShowUserInfo(true);
        setShowUserTable(false);
    }
    const handleShowTable = () => {
        setShowUserInfo(false);
        setShowUserTable(true);
    }

    return (
        <div className="d-flex flex-row  w-100 justify-content-center" >
            <div className="d-flex flex-column w-100">
                <div className="d-flex flex-column  ">
                    <div className="d-flex flex-row justify-content-left mt-5 mb-2 mx-5">
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveEmployees}>Anställda</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveCustomers}>Kunder</button>
                        </div>
                    </div>
                    <span className="d-flex flex-row justify-content-left mt-2 mb-2 mx-5 w-100 text-secondary">
                        <i>
                            {message}
                        </i>
                    </span>
                </div>
                <div className="d-flex flex-column mx-5">
                    <div className="d-flex flex-row justify-content-center">
                        {showUserTable &&
                        <table className="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Email/Användarnamn</th>
                                    <th>Namn</th>
                                    <th>Telefonnummer</th>
                                    <th>Adress</th>
                                    <th> </th>
                                    <th> </th>
                                    <th><strong>Meddelande</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                            {users.map(user =>
                                <tr key={user.id}>
                                    <UserRow
                                        id = {user.id}
                                        username = {user.username}
                                        email = {user.email}
                                        firstname = {user.firstname}
                                        lastname = {user.lastname}
                                        phone = {user.phone}
                                        address = {user.address}
                                        postalcode = {user.postalcode}
                                        city = {user.city}
                                        role = {user.role}
                                        info = {handleShowUser}
                                    />
                                </tr>
                            )}
                            </tbody>
                            <tfoot>
                            Städa Fint AB
                            </tfoot>
                        </table>
                        }
                        {/*<table>*/}
                        {/*    <tbody>*/}
                        {/*    {showUserInfo &&*/}
                        {/*    <tr className="d-flex flex-column mx-5">*/}
                        {/*        {users.map(user =>*/}
                        {/*            <td key={user.id} className="d-flex flex-row justify-content-center">*/}
                        {/*                <EditUser*/}
                        {/*                    id = {user.id}*/}
                        {/*                    username = {user.username}*/}
                        {/*                    email = {user.email}*/}
                        {/*                    firstname = {user.firstname}*/}
                        {/*                    lastname = {user.lastname}*/}
                        {/*                    phone = {user.phone}*/}
                        {/*                    address = {user.address}*/}
                        {/*                    postalcode = {user.postalcode}*/}
                        {/*                    city ={user.city}*/}
                        {/*                    role = {user.role}*/}
                        {/*                />*/}
                        {/*            </td>*/}
                        {/*        )}*/}
                        {/*    </tr>*/}
                        {/*    }*/}
                        {/*    </tbody>*/}
                        {/*</table>*/}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UserTable;
