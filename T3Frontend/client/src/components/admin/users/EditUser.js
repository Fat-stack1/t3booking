import React, {useEffect, useState} from 'react';
import AdminService from "../../../services/admin.service";

const EditUser = props => {
    const {
        id,
        username,
        email,
        firstname,
        lastname,
        phone,
        address,
        postalcode,
        city,
        role
    } = props;

    const initialUserState = {
        id: id,
        username: username,
        email: email,
        firstname: firstname,
        lastname: lastname,
        phone: phone,
        address: address,
        postalcode: postalcode,
        city: city,
        role: role
    }
    const [chosenUser, setChosenUser] = useState(initialUserState);
    const [message, setMessage] = useState("");

    const getUser = id => {
        AdminService.getUserById(id)
            .then(response => {
                setChosenUser(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getUser(props.id)
    }, [props.id])

    const handleInputChange = event => {
        const { name, value } = event.target;
        setChosenUser({ ...chosenUser, [name]: value });
    };

    const updateUser = () => {
        AdminService.update(chosenUser.id, chosenUser)
            .then(response => {
                console.log(response.data);
                setMessage("Uppdaterad!");
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <>
            <table className="table table-striped border">
                <tbody>
                <tr>
                    <th>
                        <strong>Kund-ID</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="id"
                            name="id"
                            value={chosenUser.id}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Roll</strong>
                    </td>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="role"
                            name="role"
                            value={chosenUser.role}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Användarnamn</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="username"
                            name="username"
                            value={chosenUser.username}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Mail</strong>
                    </th>
                    <td>

                        <input
                            type="text"
                            className="form-control my-3"
                            id="email"
                            name="email"
                            value={chosenUser.email}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Förnamn</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="firstname"
                            name="firstname"
                            value={chosenUser.firstname}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Efternamn</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="lastname"
                            name="lastname"
                            value={chosenUser.lastname}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Telefonnummer</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="phone"
                            name="phone"
                            value={chosenUser.phone}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Adress</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="address"
                            name="address"
                            value={chosenUser.address}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Postnummer</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="postalcode"
                            name="postalcode"
                            value={chosenUser.postalcode}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Stad</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control my-3"
                            id="city"
                            name="city"
                            value={chosenUser.city}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td className="d-flex justify-content-center">
                        <button
                            type="submit"
                            className="btn btn-info mx-1 my-2"
                            onClick={updateUser}>
                            Uppdatera
                        </button>
                    </td>
                </tr>
                </tbody>
                <tfoot>
                    <strong className="d-flex justify-content-center text-info">{message}</strong>
                </tfoot>
            </table>
        </>
    );
}

export default EditUser;