import React, { useState, useEffect } from "react";
import AdminService from "../../../services/admin.service";
import Modal from "react-bootstrap/Modal";
import EditUser from "./EditUser";
import DeleteIcon from '@material-ui/icons/DeleteTwoTone';
import InfoIcon from '@material-ui/icons/InfoTwoTone';

const UserRow = props => {
    const {
        id,
        username,
        email,
        firstname,
        lastname,
        phone,
        address,
        postalcode,
        city,
        role
    } = props;

    const initialUserState = {
        id: id,
        username: username,
        email: email,
        firstname: firstname,
        lastname: lastname,
        phone: phone,
        address: address,
        postalcode: postalcode,
        city: city,
        role: role
    };
    const [chosenUser, setChosenUser] = useState(initialUserState);
    const [message, setMessage] = useState("");

    const [showModalDelete, setShowModalDelete] = useState(false);
    const [showModalEdit, setShowModalEdit] = useState(false);

    const handleCloseModalDelete = () => {
        setShowModalDelete(false)
        getUser(id);
    };
    const handleShowModalDelete = () => setShowModalDelete(true);

    const handleCloseModalEdit = () => {
        setShowModalEdit(false);
        getUser(id);
    }
    const handleShowModalEdit = () => setShowModalEdit(true);

    useEffect(() => {
        getUser(props.id);

    }, [props.id]);

    const getUser = id => {
        AdminService.getUserById(id)
            .then(response => {
                setChosenUser(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const deleteUser = () => {
        AdminService.remove(chosenUser.id)
            .then(response => {
                console.log(response.data);
                handleCloseModalDelete();
                setMessage("Raderad!")
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <>
            <td>{chosenUser.id}</td>
            <td>
                {chosenUser.email}
                {/*<br/>*/}
                {/*{chosenUser.username}*/}
            </td>
            <td>
                {chosenUser.firstname} {chosenUser.lastname}
            </td>
            <td>{chosenUser.phone}</td>
            <td>
                {chosenUser.address}
                <br/>
                {chosenUser.postalcode}
                <br/>
                {chosenUser.city}
            </td>
            <td>
                <button
                    type="submit"
                    className="btn btn-outline-dark mx-1 my-3"
                    data-toggle="tooltip"
                    data-placement={"bottom"}
                    title={"Visa/uppdatera användare"}
                    onClick={handleShowModalEdit}>
                    <InfoIcon/>
                </button>
            </td>
            <td>
                <button className="btn btn-outline-danger mx-1 my-3" data-toggle="tooltip" data-placement={"bottom"} title={"Radera användare"} onClick={handleShowModalDelete}>
                    <DeleteIcon/>
                </button>
            </td>
            <td>
                <strong className="text-danger">{message}</strong>
            </td>
            <>
                <Modal show={showModalDelete} onHide={handleCloseModalDelete}>
                    <Modal.Header closeButton>
                        <Modal.Title>Radera Användare?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Är du säker på att du vill radera användaren?
                        Åtgärden är <strong className={"text-danger"}>permanent</strong> och går inte att ångra.
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalDelete}>Nej</button>
                        <button className="btn btn-danger" onClick={deleteUser}>Ja, radera nu!</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={showModalEdit} onHide={handleCloseModalEdit}>
                    <Modal.Header closeButton>
                        <Modal.Title>Användarinformation</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex flex-row justify-content-center">
                            <EditUser
                                id={id}
                                username={username}
                                email={email}
                                firstname={firstname}
                                lastname={lastname}
                                address={address}
                                postalcode={postalcode}
                                city={city}
                                phone={phone}
                                role={role}
                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalEdit}>Stäng</button>
                    </Modal.Footer>
                </Modal>
            </>
        </>
    );
};

export default UserRow;
