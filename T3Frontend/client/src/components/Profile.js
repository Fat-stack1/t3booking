import React, { useRef, useState} from "react";
import { useHistory } from "react-router-dom";
import AuthService from "../services/auth.service";
import UserService from "../services/user.service";

import logo from "../resources/user_img.png";
import Input from "react-validation/build/input";
import Form from "react-validation/build/form";
import FaceIcon from '@material-ui/icons/FaceTwoTone';
import InfoIcon from '@material-ui/icons/InfoTwoTone';
import esp5b from "../resources/esp-5-b.png";

const Profile = () => {
    const [currentUser, setCurrentUser] = useState(AuthService.getCurrentUser());
    let history = useHistory();

    const form = useRef();

    const deleteUserFromProfile = () => {
        UserService.deleteProfile(currentUser.id).then(response => {
            console.log(response.data);
            history.push("/");
            setCurrentUser(undefined)
        }).catch(e => {
            console.log(e);
        });
    };

    return (
        <div className="container">
            <div className="d-flex flex-column my-5 border rounded p-5 border-dark" style={{"background" : "#e0e0eb"}} >
                <div className="d-flex flex-row justify-content-evenly">
                    <div className="d-flex flex-column rounded mx-5">
                        <div className="d-flex border rounded border-dark" style={{"background": "#efeff5"}}>
                            <img className="mx-2 " style={{"width": "128px", "height": "128px"}} src={logo}
                                 alt="Dammtussen-logo"/>
                        </div>

                        <div className="d-flex flex-column my-3">
                            {/*<button className="my-3 btn btn-dark">*/}
                            {/*    Uppdatera*/}
                            {/*</button>*/}

                            {/*{currentUser.roles.includes("ROLE_CUSTOMER") &&*/}
                            {/*    <button className="my-3 btn btn-danger" onClick={deleteUserFromProfile}>*/}
                            {/*        Radera profil*/}
                            {/*    </button>*/}
                            {/*}*/}
                            <div className="d-flex justify-content-center">
                                <h3>
                                    <FaceIcon className={""}/> <strong>{currentUser.username}</strong>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex flex-column">
                        <Form ref={form}>
                            <div className={"d-flex flex-row"}>
                                <div className={"d-flex flex-column"}>
                                    <div className="d-flex flex-row">
                                            <div className="d-flex">
                                                <div className="form-group mb-2">
                                                    <label htmlFor="user_id">Användar-ID</label>
                                                    <Input
                                                        type="text"
                                                        name="user_id"
                                                        readOnly
                                                        value={currentUser.id}
                                                    />
                                                </div>
                                            </div>
                                        <div className="d-flex mx-3">
                                            <div className="form-group mb-2 ">
                                                <label className="d-flex" htmlFor="user_city">Roll</label>
                                                {currentUser.roles && currentUser.roles.map((role, index) =>
                                                    <div
                                                        className="bg-white border border-secondary px-1"
                                                        style={{"width" : "190px", "paddingTop": "2px"}}
                                                        key={{index}}>{role}
                                                    </div>)}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex flex-row">
                                        <div className="form-group mb-2 d-flex">
                                            <div className="d-flex flex-column">
                                                <label className="d-flex" htmlFor="user_firstname">Förnamn</label>
                                                <Input
                                                    type="text"
                                                    className="d-flex"
                                                    name="user_firstname"
                                                    readOnly
                                                    value={currentUser.firstname}
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group mb-2 d-flex mx-3">
                                            <div className="d-flex flex-column">
                                                <label className="d-flex" htmlFor="user_lastname">Efternamn</label>
                                                <Input
                                                    type="text"
                                                    className="d-flex"
                                                    name="user_lastname"
                                                    readOnly
                                                    value={currentUser.lastname}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <img src={esp5b} style={{ "width": "128px", "height": "128px" }} alt={"Artist: Elsa Söderpalm Patel"}/>
                                </div>
                            </div>
                            <div className="d-flex flex-row">
                                <div className="form-group mb-2 d-flex">
                                    <div className="d-flex flex-column">
                                        <label className="d-flex" htmlFor="user_address">Adress</label>
                                        <Input
                                            type="text"
                                            className="d-flex"
                                            name="user_address"
                                            readOnly
                                            value={currentUser.address}
                                        />
                                    </div>
                                </div>
                                <div className="form-group mb-2 d-flex mx-3">
                                    <div className="d-flex flex-column">
                                        <label className="d-flex" htmlFor="user_postalcode">Postnummer</label>
                                        <Input
                                            type="text"
                                            className="d-flex"
                                            name="user_postalcode"
                                            readOnly
                                            value={currentUser.postalcode}
                                        />
                                    </div>
                                </div>
                                <div className="form-group mb-2 d-flex">
                                    <div className="d-flex flex-column">
                                        <label className="d-flex" htmlFor="user_city">Stad</label>
                                        <Input
                                            type="text"
                                            className="d-flex"
                                            name="user_city"
                                            readOnly
                                            value={currentUser.city}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
                <span className={"text-secondary d-flex flex-row justify-content-center"}>
                    <i className={"small mx-3"}>
                        <InfoIcon/> Vi behandlar dina personuppgifter i linje med GDPR. Var god kontakta supportcentret vid frågor gällande hantering av dina personuppgifter via <a className={"text-info"}>support@stadafintab.se</a>.
                    </i>
                </span>
            </div>
        </div>
    );
};

export default Profile;