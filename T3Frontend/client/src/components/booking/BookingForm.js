import React, { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import BookingService from "../../services/booking.service";
import AuthService from "../../services/auth.service";
import TimeOption from "./DropDowns/TimeOption";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const BookingForm = (props) => {
    const {calendarDate} = props;

    const initialBookingState = {
        id: null,
        customer_id: null,
        cleaning_service_id: 1,
        time: "09:00"
    };

    const form = useRef();
    const checkBtn = useRef();
    const [newBooking, setNewBooking] = useState(initialBookingState);
    const currentUser = AuthService.getCurrentUser(false);
    const [successful, setSuccessful] = useState(false);
    const [message, setMessage] = useState("");
    

    const handleInputChange = event => {
        const { name, value } = event.target;
        setNewBooking({ ...newBooking, [name]: value });
    };

    const handleRegisterBooking = (e) => {
       e.preventDefault();

        setMessage("");
        setSuccessful(false);

        form.current.validateAll();
                
            BookingService.createBooking(currentUser.id, newBooking.cleaning_service_id, calendarDate, newBooking.time)
                .then(() => {
                    setSuccessful(true);
                    setMessage("Bokning lagd!");
                },
                (error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessage(resMessage);
                    setSuccessful(false);
                }
            );
    };

    return (
        <div className="col-md-12">
            <div>
                <Form onSubmit={handleRegisterBooking} ref={form}>
                    {!successful && (
                        <div>
                            <div className="form-group mb-2">
                                <label htmlFor="customer_id">Kund-ID</label>
                                <Input
                                    type="text"
                                    className="form-control d-flex "
                                    name="customer_id"
                                    readOnly
                                    value={currentUser.id}
                                    validations={[required]}
                                />
                            </div>
                             <div className="d-flex flex-column">
                                <label className="mt-3 d-flex">Städtjänst</label>
                                    <select
                                        className="form-control d-flex "
                                        name="cleaning_service_id"
                                        value={newBooking.cleaning_service_id}
                                        onChange={handleInputChange}
                                     >
                                        <option value={1}>Basic Städning</option>
                                        <option value={2}>Topp Städning</option>
                                        <option value={3}>Diamant Städning</option>
                                        <option value={4}>Fönstertvätt</option>
                                    </select>
                                </div> 
                                <div className="d-flex flex-column">
                                <label className="mt-3 d-flex">Tid</label>
                                    <TimeOption
                                        time = {newBooking.time}
                                        inputChange = {handleInputChange}
                                    />
                                </div> 
                                <div className="form-group mb-2 my-3" >
                                <label htmlFor="date">Datum</label>
                                <Input
                                    className="form-control d-flex "
                                    type="text"
                                    name="date"
                                    value={calendarDate}                                    
                                    validations={[required]}
                                />
                            </div>
                       
                            <div className="form-group mt-4">
                                <button className="btn btn-primary btn-block">Boka</button>
                            </div>
                        </div>
                    )}

                    {message && (
                        <div className="form-group">
                            <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert" >
                                {message}
                            </div>
                        </div>
                    )}
                    <CheckButton style={{ display: "none" }} ref={checkBtn} />
                </Form>
            </div>
        </div>
    );
};

export default BookingForm;