import React, {useEffect, useState} from 'react';
import BookingService from "../../services/booking.service";
import StatusOption from "./DropDowns/StatusOption";
import AuthService from "../../services/auth.service";
import CleaningOption from "./DropDowns/CleaningOption";
import EmployeeOption from "./DropDowns/EmployeeOption";
import TimeOption from "./DropDowns/TimeOption";
import CustomerComment from "./CustomerComment";
import dayjs from 'dayjs';
import InfoIcon from "@material-ui/icons/InfoTwoTone";

const EditBooking = props => {
    const initialBookingState = {
        id: null,
        customer_id: null,
        employee_id: null,
        cleaning_service_id: null,
        date: "",
        time: props.time,
        timestamp: undefined,
        status: undefined,
        details: "",
        comment: ""
    };

    const [currentBooking, setCurrentBooking] = useState(initialBookingState);
    const [message, setMessage] = useState("");

    const [currentUser] = useState(AuthService.getCurrentUser());

    const [showAdminActions, setShowAdminActions] = useState(false);
    // const [showEmployeeActions, setShowEmployeeActions] = useState(false);
    // const [showCustomerActions, setShowCustomerActions] = useState(false);

    const getBooking = id => {
        BookingService.getBookingById(id)
            .then(response => {
                setCurrentBooking(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getBooking(props.id);
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            setShowAdminActions(true);
            // setShowCustomerActions(false);
            // setShowCustomerActions(false);
        }
        // if (currentUser.roles.includes("ROLE_EMPLOYEE")){
        //     setShowAdminActions(false);
        //     setShowEmployeeActions(true);
        //     setShowCustomerActions(false);
        // }
        // if (currentUser.roles.includes("ROLE_CUSTOMER")){
        //     setShowAdminActions(false);
        //     setShowEmployeeActions(false);
        //     setShowCustomerActions(true);
        // }

    }, [props.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentBooking({ ...currentBooking, [name]: value });
    };

    const updateBooking = () => {
        BookingService.update(currentBooking.id, currentBooking)
            .then(response => {
                console.log(response.data);
                setMessage("Uppdaterad!");
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <>
            <table className="table table-striped border">
                <tbody>
                <tr>
                    <th>
                        <strong>Boknings-ID</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control "
                            id="id"
                            name="id"
                            value={currentBooking.id}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Kund-ID</strong>
                    </th>
                    <td>
                        <input
                            type="text"
                            className="form-control "
                            id="customer_id"
                            name="customer_id"
                            value={currentBooking.customer_id}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Städtjänst</strong>
                    </th>
                    <td>
                        <CleaningOption
                            item = {currentBooking.cleaning_service_id}
                            inputChange = {handleInputChange}
                        />
                    </td>
                </tr>
                {showAdminActions &&
                    <tr>
                        <th>
                            <strong>Tilldelad anställd</strong>
                        </th>
                        <td>
                            <EmployeeOption
                                employee_id = {currentBooking.employee_id}
                                inputChange = {handleInputChange}
                            />
                        </td>
                    </tr>
                }
                <tr>
                    <th>
                        <strong>Status</strong>
                        <button
                            className={"btn"}
                            data-toggle="tooltip"
                            data-placement={"bottom"}
                            title={"Möjliga åtgärder syns här när det blir aktuellt"}>
                        <InfoIcon />
                    </button>
                    </th>
                    <td>
                        <StatusOption
                            status = {currentBooking.status}
                            inputChange = {handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Datum</strong>
                    </th>
                    <td>
                        <input
                            type="date"
                            className="form-control "
                            id="date"
                            name="date"
                            value={dayjs(currentBooking.date).format('YYYY-MM-DD')}
                            onChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Tid</strong>
                    </th>
                    <td>
                        <TimeOption
                            time = {currentBooking.time}
                            inputChange={handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <th><strong>Tidstämpel</strong></th>
                    <td>
                        {currentBooking.timestamp}
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Kundkommentar</strong>
                    </th>
                    <td>
                        <CustomerComment
                            comment = {currentBooking.comment}
                            inputChange = {handleInputChange}
                        />
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td className="d-flex justify-content-center align-content-center">
                        <button
                            type="submit"
                            className="btn btn-info mx-1 my-3"
                            onClick={updateBooking}>
                            Uppdatera
                        </button>
                    </td>
                </tr>

                </tbody>
                <tfoot>
                    <strong className="d-flex text-center text-info"> {message} </strong>
                </tfoot>
            </table>
        </>
    );
}

export default EditBooking;
