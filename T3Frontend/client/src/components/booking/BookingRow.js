import React, { useState, useEffect } from "react";
import BookingService from "../../services/booking.service";
import Modal from "react-bootstrap/Modal";
import RateReviewIcon from '@material-ui/icons/RateReviewTwoTone';
import dayjs from 'dayjs';
import EditBooking from "./EditBooking";
import InfoIcon from "@material-ui/icons/InfoTwoTone";
import CancelIcon from '@material-ui/icons/CancelTwoTone';

const BookingRow = props => {
    const { id,
        customer_id,
        employee_id,
        cleaning_service_id,
        date,
        time,
        status,
        comment
    } = props;

    const initialBookingState =
    {
        id: id,
        customer_id: customer_id,
        employee_id: employee_id,
        cleaning_service_id: cleaning_service_id,
        date: date,
        time: time,
        timestamp: "",
        status: status,
        comment: comment
    };
    const [currentBooking, setCurrentBooking] = useState(initialBookingState);
    const [message, setMessage] = useState("");

    const [commentExists, setCommentExists] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [showModalEdit, setShowModalEdit] = useState(false);

    const handleCloseModalDelete = () => {
        setShowModalDelete(false)
        getBooking(id);
    };
    const handleShowModalDelete = () => setShowModalDelete(true);

    const handleCloseModalEdit = () => {
        setShowModalEdit(false);
        getBooking(id);
    }
    const handleShowModalEdit = () => setShowModalEdit(true);

    useEffect(() => {
        getBooking(id);
        handleCommentExists();
    }, [id]);

    const getBooking = id => {
        BookingService.getBookingById(id)
            .then(response => {
                setCurrentBooking(response.data);
                console.log(response.data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    const deleteBooking = () => {
        BookingService.remove(currentBooking.id)
            .then(response => {
                console.log(response.data);
                handleCloseModalDelete();
                setMessage("Avbokad!")
            })
            .catch(e => {
                console.log(e);
            });
    };

    const handleCommentExists = () => {
        if (props.comment != null) {
            setCommentExists(true);
        } else {
            setCommentExists(false);
        }
    }

    return (
        <>
            <td>{currentBooking.id}</td>
            <td>{currentBooking.customer_id}</td>
            <td>{currentBooking.employee_id}</td>
            <td>{currentBooking.cleaning_service_id}</td>
            <td>{dayjs(currentBooking.date).format('YYYY-MM-DD')}</td>
            <td>{currentBooking.time}</td>
            <td>{currentBooking.status}</td>
            <td>
                <button
                    type="submit"
                    className="btn btn-outline-dark mx-1 my-1"
                    data-toggle="tooltip"
                    data-placement={"bottom"}
                    title={"Visa/uppdatera bokning"}
                    onClick={handleShowModalEdit}>
                    <InfoIcon/>
                </button>
            </td>
            <td>
                {props.cancelBtn ?
                <button
                    className=" btn btn-outline-danger mx-1 my-1"
                    data-toggle="tooltip"
                    data-placement={"bottom"}
                    title={"Avboka städtjänst"}
                    onClick={handleShowModalDelete}>
                    <CancelIcon/>
                </button> : <></> }
            </td>
            <td>
                {commentExists ? (
                    <button
                        className={"btn"}
                        data-toggle="tooltip"
                        data-placement={"bottom"}
                        title={currentBooking.comment}>
                        <RateReviewIcon />
                    </button>
                    ) : (<></>) }
            </td>
            <td>
                <strong className="text-danger">{message}</strong>
            </td>
            <>
                <Modal show={showModalDelete} onHide={handleCloseModalDelete}>
                    <Modal.Header closeButton>
                        <Modal.Title>Avboka bokning?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Är du säker på att du vill avboka den valda bokningen?
                        Åtgärden är <strong className={"text-danger"}>permanent</strong> och går inte att ångra.
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalDelete}>Nej</button>
                        <button className="btn btn-danger" onClick={deleteBooking}>Ja, avboka nu!</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={showModalEdit} onHide={handleCloseModalEdit}>
                    <Modal.Header closeButton>
                        <Modal.Title>Bokningsdetaljer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex flex-row justify-content-center">
                            <EditBooking
                                id={id}
                                customer_id={customer_id}
                                employee_id={employee_id}
                                cleaning_service_id={cleaning_service_id}
                                date={date}
                                time={time}
                                status={status}
                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalEdit}>Stäng</button>
                    </Modal.Footer>
                </Modal>
            </>
        </>
    );
};

export default BookingRow;
