import React, { useState, useEffect, useRef } from "react";
import BookingService from "../../services/booking.service";
import AuthService from "../../services/auth.service";

import BookingCalendar from "./BookingCalendar";
import BookingRow from "./BookingRow";
import DeleteIcon from "@material-ui/icons/DeleteTwoTone";
import Modal from "react-bootstrap/Modal";

const BookingTable = () => {
    const [currentUser] = useState(AuthService.getCurrentUser());

    const [bookingRow, setBookingRow] = useState([]);
    const [message, setMessage] = useState("");

    const [showAdminBooking, setShowAdminBooking] = useState(false);
    const [showCustomerBooking, setShowCustomerBooking] = useState(false);
    const [showEmployeeBooking, setShowEmployeeBooking] = useState(false);

    const [showBookingTable, setShowBookingTable] = useState(true);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [showCancelBtn, setShowCancelBtn] = useState(true);

    const bookingsRef = useRef();
    bookingsRef.current = bookingRow;

    const handleCloseModalDelete = () => {
        setShowModalDelete(false)
    };
    const handleShowModalDelete = () => setShowModalDelete(true);

    useEffect(() => {
        // retrieveAllBookings();
        setMessage("Klicka på kategorierna för att visa valda bokningar")
    }, []);

    const retrieveAllBookings = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getBookings()
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                    setShowCancelBtn(true);
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getBookingByEmployeeId(currentUser.id).then(response => {
                setBookingRow(response.data);
                handleShowTable();
                setShowCancelBtn(true);
            }).catch((e) => {
                console.log(e);
            });
        } else {
            setMessage("Något gick fel :(");
        }
        handleShowCancelBtn();
    };

    const retrieveUnconfirmedBookings = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getAdminBookingByStatus("OBEKRÄFTAD")
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
            BookingService.getCustomerBookingByStatus(currentUser.id, "OBEKRÄFTAD").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getEmployeeBookingByStatus(currentUser.id, "OBEKRÄFTAD").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else {setMessage("Något gick fel :(")}
        handleShowCancelBtn();
    };

    const retrieveConfirmedBookings = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getAdminBookingByStatus("BEKRÄFTAD")
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
            BookingService.getCustomerBookingByStatus(currentUser.id, "BEKRÄFTAD").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getEmployeeBookingByStatus(currentUser.id, "BEKRÄFTAD").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else {setMessage("Något gick fel :(")}
        handleShowCancelBtn();
    };

    const retrieveOngoingBookings = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getAdminBookingByStatus("UNDER_UTFÖRANDE")
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
            BookingService.getCustomerBookingByStatus(currentUser.id, "UNDER_UTFÖRANDE").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getEmployeeBookingByStatus(currentUser.id, "UNDER_UTFÖRANDE").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else {setMessage("Något gick fel :(")}
        handleCancelBtn();
    };

    const retrieveBookingHistory = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getAdminBookingByStatus("UTFÖRT")
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
            BookingService.getCustomerBookingByStatus(currentUser.id, "UTFÖRT").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getEmployeeBookingByStatus(currentUser.id, "UTFÖRT").then(response => {
                setBookingRow(response.data);
                handleShowTable();
            }).catch((e) => {
                console.log(e);
            });
        } else {setMessage("Något gick fel :(")}
        handleCancelBtn();
    };

    const retrieveApprovedBookings = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getAdminBookingByStatus("GODKÄNT")
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
            BookingService.getCustomerBookingByStatus(currentUser.id, "GODKÄNT").then(response => {
                setBookingRow(response.data);
                handleShowTable();

            }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getEmployeeBookingByStatus(currentUser.id, "GODKÄNT").then(response => {
                setBookingRow(response.data);
                handleShowTable();

            }).catch((e) => {
                console.log(e);
            });
        } else {setMessage("Något gick fel :(")}
        handleCancelBtn();
    };

    const retrieveUnapprovedBookings = () => {
        if (currentUser.roles.includes("ROLE_ADMIN")) {
            handleIsAdmin();
            BookingService.getAdminBookingByStatus("UNDERKÄND")
                .then((response) => {
                    setBookingRow(response.data);
                    handleShowTable();
                }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
            BookingService.getCustomerBookingByStatus(currentUser.id, "UNDERKÄND").then(response => {
                setBookingRow(response.data);
                handleShowTable();

            }).catch((e) => {
                console.log(e);
            });
        } else if (currentUser.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
            BookingService.getEmployeeBookingByStatus(currentUser.id, "UNDERKÄND").then(response => {
                setBookingRow(response.data);
                handleShowTable();

            }).catch((e) => {
                console.log(e);
            });
        } else {setMessage("Något gick fel :(")}
        handleCancelBtn();
    };

    const removeAllBookings = () => {
        BookingService.removeAll()
            .then((response) => {
                console.log(response.data);
                handleCloseModalDelete();
                refreshList();
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const refreshList = () => {
        retrieveAllBookings();
    };

    const handleIsAdmin = () => {
        setShowAdminBooking(true);
        setShowCustomerBooking(false);
        setShowEmployeeBooking(false);
    };
    const handleIsCustomer = () => {
        setShowAdminBooking(false);
        setShowCustomerBooking(true);
        setShowEmployeeBooking(false);
    };
    const handleIsEmployee = () => {
        setShowAdminBooking(false);
        setShowCustomerBooking(false);
        setShowEmployeeBooking(true);
    };

    const handleShowTable = () => {
        setShowBookingTable(true);
    };

    const handleCancelBtn = () => {
        setShowCancelBtn(false);
    }
    const handleShowCancelBtn = () => {
        setShowCancelBtn(true);
    }

    return (
        <div className="d-flex flex-row w-100 justify-content-center" >
            <div className="d-flex flex-column w-100">
                <div className="d-flex flex-column ">
                    <div className="d-flex flex-row justify-content-left mt-5 mb-2 mx-5 w-100">
                        {!currentUser.roles.includes("ROLE_CUSTOMER") &&
                            <div className="d-flex mx-2">
                                <button className="btn btn btn-dark" onClick={retrieveAllBookings}>Alla</button>
                            </div>
                        }
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-secondary" onClick={retrieveUnconfirmedBookings}>Obekräftade</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveConfirmedBookings}>Bekräftade</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveOngoingBookings}>Pågående</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveBookingHistory}>Historik</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-dark" onClick={retrieveApprovedBookings}>Godkända</button>
                        </div>
                        <div className="d-flex mx-2">
                            <button className="btn btn btn-danger" onClick={retrieveUnapprovedBookings}>Underkända</button>
                        </div>
                        {currentUser.roles.includes("ROLE_ADMIN") &&
                        <div className="d-flex mx-2">
                            <button
                                className="btn btn btn-outline-danger"
                                data-toggle="tooltip"
                                data-placement={"bottom"}
                                title={"Töm bokningslistan"}
                                onClick={handleShowModalDelete}>
                                <DeleteIcon/>
                            </button>
                        </div>
                        }
                    </div>
                    <span className="d-flex flex-row justify-content-left mt-2 mb-2 mx-5 w-100 text-secondary">
                        <i>
                            {message}
                        </i>
                    </span>
                </div>
                <div className="d-flex flex-column mx-5">
                    <div className="d-flex flex-row justify-content-center">
                        {showBookingTable &&
                        <table className="table text-center table-striped">
                            <thead>
                                <tr>
                                    <th>Boknings-ID</th>
                                    <th>Kund-ID</th>
                                    <th>Anställnings-ID</th>
                                    <th>Städtjänst-ID</th>
                                    <th>Datum</th>
                                    <th>Tid</th>
                                    <th>Status</th>
                                    <th></th>
                                    <th></th>
                                    <th>Kundkommentar</th>
                                    <td><strong>Meddelande</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            {bookingRow ? (bookingRow.map(bookingRow =>
                                <tr key={bookingRow.id}>
                                    <BookingRow
                                        id={bookingRow.id}
                                        customer_id={bookingRow.customer_id}
                                        cleaning_service_id={bookingRow.cleaning_service_id}
                                        date={bookingRow.date}
                                        time={bookingRow.time}
                                        status={bookingRow.status}
                                        comment={bookingRow.comment}
                                        cancelBtn={showCancelBtn}
                                    />
                                </tr>
                            )) : (
                                <tr>
                                    <h6>DET FINNS INGA BOKNINGAR I SYSTEMET</h6>
                                </tr>
                            )}
                            </tbody>
                            <tfoot>
                                Städa Fint AB
                            </tfoot>
                        </table>
                        }
                    </div>
                </div>
            </div>
            <>
                <Modal show={showModalDelete} onHide={handleCloseModalDelete}>
                    <Modal.Header closeButton>
                        <Modal.Title>Radera ALLA bokningar?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Är du säker på att du vill radera samtliga bokningar i systemet?
                        Åtgärden är <strong className={"text-danger"}>permanent</strong> och går inte att ångra.
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-dark" onClick={handleCloseModalDelete}>Nej</button>
                        <button className="btn btn-danger" onClick={removeAllBookings}>Ja, radera nu!</button>
                    </Modal.Footer>
                </Modal>
            </>
        </div>
    );
};
export default BookingTable;
