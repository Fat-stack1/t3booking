import React, {useEffect, useState} from "react";
import AuthService from "../../services/auth.service";
const CustomerComment = (props) => {
    const [isAdmin, setIsAdmin] = useState();
    const [isEmployee, setIsEmployee] = useState();
    const [isCustomer, setIsCustomer] = useState();

    useEffect(()=> {
        const user = AuthService.getCurrentUser();

        if(user.roles.includes("ROLE_ADMIN")){
           handleIsAdmin();
        }
        if (user.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
        }
        if (user.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
        }
    },[])

    const handleIsAdmin = () => {
        setIsCustomer(false);
        setIsAdmin(true);
        setIsEmployee(false);
    }
    const handleIsEmployee = () => {
        setIsCustomer(false);
        setIsAdmin(false);
        setIsEmployee(true);
    }
    const handleIsCustomer = () => {
        setIsCustomer(true);
        setIsAdmin(false);
        setIsEmployee(false);
    }

    return(
        <>
            {(isAdmin || isEmployee) &&
            <div>{props.comment}</div>
            }
            {isCustomer && !isAdmin && !isEmployee &&
                <input
                    type="text"
                    className="form-control"
                    id="comment"
                    name="comment"
                    placeholder={"Lämna en kommentar här"}
                    value={props.comment}
                    onChange={props.inputChange}
                    />
            }
        </>
    )
}
export default CustomerComment;