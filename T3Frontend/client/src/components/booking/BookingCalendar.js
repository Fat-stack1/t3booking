import React, {useState} from "react";
import Calendar from "react-calendar";
import BookingForm from "./BookingForm";
import dayjs from 'dayjs';

const BookingCalendar = () => {

    const [date, setDate] = useState(new Date());

    return (
        <div className="d-flex flex-column mt-4">
            <div className="d-flex flex-column text-center">
                <h2>Välj en av våra förmånliga städtjänster!</h2>
                <h5>Priserna du ser längst ner på sidan gäller per h</h5>
            </div>
            <div className="d-flex flex-row justify-content-center my-2 py-3">
                <div className="mx-4 d-flex">
                    <Calendar
                        onChange={setDate}
                        value = {date}
                        minDate={new Date()}
                    />
                </div>

                <div>
                    <>
                        <BookingForm
                            calendarDate = {dayjs(date).format('YYYY-MM-DD')}
                        />
                    </>
                </div>
            </div>
        </div>

    )
}

export default BookingCalendar;