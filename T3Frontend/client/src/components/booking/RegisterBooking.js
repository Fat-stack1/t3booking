import React, { useState, useEffect } from "react";

import  UserService from '../../services/user.service';
import EventBus from '../../common/EventBus';
import BookingCalendar from "./BookingCalendar";
import CleaningCard from "../public/CleaningCard";

const BoardUser = () => {
    const [content, setContent] = useState("");

    useEffect(() => {
        UserService.getBookingBoard().then(
            (response) => {
                setContent(response.data);
            },
            (error) => {
                const _content =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                setContent(_content);

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            }
        );
    }, []);

    return (
        <div className="container">
            <header>
                <h3>{content}</h3>
                <BookingCalendar/>
            </header>
            <div className="d-flex flex-column justify-content-center align-items-center">
                <ul className="d-flex flex-column justify-content-center">
                    <CleaningCard/>
                </ul>
            </div>

        </div>
    );
}

export default BoardUser;