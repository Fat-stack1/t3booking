import React, {useEffect, useState} from "react";
import AuthService from "../../../services/auth.service";

const StatusOption = props => {
    // const initialBookingState = {
    //     id: null,
    //     customer_id: null,
    //     employee_id: null,
    //     cleaning_service_id: null,
    //     date: "",
    //     time: "",
    //     timestamp: undefined,
    //     status: undefined
    // };
    // const [currentBooking, setCurrentBooking] = useState(initialBookingState);
    const [showAdminOption, setShowAdminOption] = useState(false);
    const [showCustomerOption, setShowCustomerOption] = useState(false);
    const [showEmployeeOption, setShowEmployeeOption] = useState(false);

    useEffect(() => {
        const user = AuthService.getCurrentUser();
        if (user.roles.includes("ROLE_ADMIN")){
            handleIsAdmin();
        } else if (user.roles.includes("ROLE_EMPLOYEE")) {
            handleIsEmployee();
        } else if (user.roles.includes("ROLE_CUSTOMER")) {
            handleIsCustomer();
        }
    })

    const handleIsAdmin = () => {
        setShowAdminOption(true);
        setShowCustomerOption(false);
        setShowEmployeeOption(false);
    };

    const handleIsCustomer = () => {
        setShowAdminOption(false);
        setShowCustomerOption(true);
        setShowEmployeeOption(false);
    };

    const handleIsEmployee = () => {
        setShowAdminOption(false);
        setShowCustomerOption(false);
        setShowEmployeeOption(true);
    };

    return(
        <>
            {showAdminOption &&
                <select
                    className="form-control d-flex "
                    id="status"
                    name="status"
                    value={props.status}
                    onChange={props.inputChange} >
                    <option className="text-center" value="none" selected="selected"> -- Ändra status --</option>
                    <option value={0}>Obekräftad</option>
                    <option value={1}>Bekräftad</option>
                    <option value={2}>Bokad</option>
                    <option value={3}>Under utförande</option>
                    <option value={4}>Utförd</option>
                    <option value={5}>Godkänd</option>
                    <option value={6}>Fakturerad</option>
                    <option value={7}>Betald</option>
                    <option value={8}>Underkänd</option>
                </select>
            }
            {showEmployeeOption &&
                <select
                    className="form-control d-flex "
                    id="status"
                    name="status"
                    value={props.status}
                    onChange={props.inputChange} >
                    <option className="text-center" value="none" selected="selected"> -- Ändra status --</option>
                    <option value={3}>Under utförande</option>
                    <option value={4}>Utförd</option>
                </select>
            }
            {showCustomerOption && (props.status === "UTFÖRT") &&
                <select
                    className="form-control d-flex "
                    id="status"
                    name="status"
                    value={props.status}
                    onChange={props.inputChange} >
                    <option className="text-center" value="none" selected="selected"> -- Ändra status --</option>
                    <option value={5}>Godkänd</option>
                    <option value={8}>Underkänd</option>
                </select>
            }
        </>
    )
}

export default StatusOption;