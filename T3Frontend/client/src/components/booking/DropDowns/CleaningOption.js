import React, {useEffect, useState} from 'react';
import UserService from "../../../services/user.service";

const CleaningOption = (props) => {
    const [cleaningList, setCleaningList] = useState([]);

    useEffect(() => {
        getCleaningList()
    }, [])

    const getCleaningList = () => {
        UserService.getCleaningServices().then((response) => {
            setCleaningList(response.data)
        });
    };

    return (
        <>
            <select
                className="form-control d-flex "
                id="cleaning_service_id"
                name="cleaning_service_id"
                value={props.item}
                onChange={props.inputChange} >
                <option className="d-flex text-center" value="none" selected="selected">
                    -- Välj städtjänst --
                </option>
                {cleaningList.map(item => {
                    return (
                        <option
                            name="id"
                            key={item.id}
                            value={item.id} >
                            {item.name.replace("_"," ")}
                        </option>
                    )
                })})
            </select>
        </>
    );
}

export default CleaningOption;