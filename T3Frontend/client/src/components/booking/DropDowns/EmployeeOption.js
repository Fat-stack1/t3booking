import React, {useEffect, useState} from "react";
import AdminService from "../../../services/admin.service";

const EmployeeOption = (props) => {
    const [employees, setEmployees] = useState([]);

    useEffect(() => {
        retrieveEmployees();
    }, [])

    const retrieveEmployees = () => {
        AdminService.getEmployeesList().then((response) => {
            setEmployees(response.data);

        }).catch((e) => {
            console.log(e);
        });
    };

    return(
        <select
            className="form-control d-flex "
            id="employee_id"
            name="employee_id"
            value={props.employee_id}
            onChange={props.inputChange} >
            <option className="d-flex text-center" value="none" selected="selected"> -- Välj anställd --</option>
            {employees.map(employee => {
                return (
                    <option
                        name="id"
                        key={employee.id}
                        value={employee.id}
                    >{employee.firstname} {employee.lastname}
                    </option>
                )
            })})
        </select>
    )
}

export default EmployeeOption;