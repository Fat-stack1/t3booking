const TimeOption = (props) => {

    return (
        <>
            <select
                className="form-control d-flex "
                name="time"
                value={props.time}
                onChange={props.inputChange} >
                <option className="d-flex text-center" value="none"> -- Välj tid --</option>
                <option value={"09:00"}>09:00</option>
                <option value={"12:00"}>12:00</option>
                <option value={"14:00"}>14:00</option>
                <option value={"16:00"}>16:00</option>
            </select>
        </>
    );
}

export default TimeOption;