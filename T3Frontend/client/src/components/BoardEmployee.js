import React, { useState, useEffect } from "react";

import AuthService from "../services/auth.service";
import UserService from "../services/user.service";
import EventBus from "../common/EventBus";
import FaceIcon from '@material-ui/icons/FaceTwoTone';
import BookingTable from "./booking/BookingTable";
import Profile from "./Profile";
import BoardUser from "./booking/RegisterBooking";
import LocalAtmIcon from '@material-ui/icons/LocalAtmTwoTone';
import HomeIcon from '@material-ui/icons/HomeTwoTone';
import LibraryAddIcon from '@material-ui/icons/LibraryAddTwoTone';
import SalaryTable from "./economy/salaries/SalaryTable";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenterTwoTone";
import AccountBalanceIcon from "@material-ui/icons/AccountBalanceTwoTone";
import WelcomeBoard from "./WelcomeBoard";

const BoardEmployee = () => {
    const user = AuthService.getCurrentUser();

    const [content, setContent] = useState("");
    const [showWelcome, setShowWelcome] = useState(true);
    const [showProfile, setShowProfile] = useState(false);
    const [showBookings, setShowBookings] = useState(false);
    const [showBookingItems, setShowBookingItems] = useState(false);
    const [showAddBooking, setShowAddBooking] = useState(false);
    const [showSalary, setShowSalary] = useState(false);

    useEffect(() => {
        UserService.getEmployeeBoard().then(
            (response) => {
                setContent(response.data);
            },
            (error) => {
                const _content =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                setContent(_content);

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            }
        );
    }, []);

    const handleViewBookings = () => {
        setShowBookings(true);
        setShowSalary(false);
        setShowWelcome(false);
        setShowProfile(false);
        setShowBookingItems(true);
        setShowAddBooking(false);
    }
    const handleViewAddBookings = () => {
        setShowBookings(false);
        setShowSalary(false);
        setShowWelcome(false);
        setShowProfile(false);
        setShowBookingItems(true);
        setShowAddBooking(true);
    }

    const handleViewSalary = () => {
        setShowBookings(false);
        setShowSalary(true);
        setShowWelcome(false);
        setShowProfile(false);
        setShowBookingItems(false);
        setShowAddBooking(false);
    }

    const handleViewWelcome = () => {
        setShowBookings(false);
        setShowSalary(false);
        setShowWelcome(true);
        setShowProfile(false);
        setShowBookingItems(false);
        setShowAddBooking(false);
    }
    const handleViewProfile = () => {
        setShowBookings(false);
        setShowSalary(false);
        setShowWelcome(false);
        setShowProfile(true);
        setShowBookingItems(false);
        setShowAddBooking(false);
    }

    return (
        <div className="d-flex flex-row vh-100" >
            <div className=" border border-left-0 border-top-0 border-bottom-0 d-flex" style={{"width":"255px"}} >
                <div className=" bg-light " style={{"width":"250px"}}>
                    <nav className="navbar navbar-expand-lg navbar-light d-flex" style={{"width":"250px"}}>
                        <div className="" id="navbarNav" style={{"width":"250px"}}>
                            <ul className="navbar-nav d-flex flex-column">
                                <li className="nav-item active d-flex border-bottom">
                                        <span className="nav-link d-flex" style={{cursor: "pointer"}} onClick={handleViewWelcome}>
                                            <div className={"mx-2"}>
                                                <HomeIcon/> <strong>{content} {user.firstname}</strong>
                                            </div>
                                        </span>
                                    </li>
                                    <li className="nav-item d-flex">
                                        <span className="nav-link d-flex" style={{cursor: "pointer"}} onClick={handleViewProfile}><div className={"mx-2"}><FaceIcon/> <i>{user.username}</i></div></span>
                                    </li>
                                    <li className="nav-item d-flex">
                                        <span className="nav-link d-flex" style={{cursor: "pointer"}} onClick={handleViewBookings}><div className={"mx-2"}><BusinessCenterIcon/> Bokningar</div></span>
                                    </li>
                                    {showBookingItems ?
                                        <ul>
                                            <li className="nav-item d-flex">
                                                <span className="nav-link d-flex" onClick={handleViewAddBookings} style={{cursor: "pointer"}}><div className={"mx-2"}><LibraryAddIcon/> <i>Ny bokning</i></div></span>
                                            </li>
                                        </ul> : <></>
                                    }
                                    <li className="nav-item d-flex">
                                        <span className="nav-link d-flex" style={{cursor: "pointer"}} onClick={handleViewSalary}><div className={"mx-2"}><AccountBalanceIcon/> Lönespec</div></span>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            <div className="d-flex w-100 justify-content-center ">
                {!showWelcome ? (<></>) : (showWelcome && <WelcomeBoard/>)}
                {!showProfile ? (<></>) : (showProfile && <Profile/>)}
                {!showBookings  ? (<></>) :(showBookings && <BookingTable/> )}
                {!showAddBooking ? ( <></> ) : (showAddBooking && <BoardUser/>) }
                {!showSalary ? (<></>) : (showSalary && <SalaryTable/>)}
            </div>
        </div>
    );
}

export default BoardEmployee;