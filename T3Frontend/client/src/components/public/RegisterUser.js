import React, { useState, useRef, useEffect } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import AuthService from "../../services/auth.service";



const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                Du måste fylla i detta fältet!
            </div>
        );
    }
};

const validEmail = (value) => {
    if (!isEmail(value)) {
        return (
            <div className="alert alert-danger" role="alert">
                Vänligen fyll i en giltig email.
            </div>
        );
    }
};

const vusername = (value) => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="alert alert-danger" role="alert">
                Användarnamnet måste innehålla minst 3 och max 20 tecken.
            </div>
        );
    }
};

const vpassword = (value) => {
    if (value.length < 3 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                Lösenordet måste innehålla minst 3 och max 40 tecken.
            </div>
        );
    }
};

const RegisterUser = () => {
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [postalcode, setPostalcode] = useState("");
    const [city, setCity] = useState("");
    const [role, setRole] = useState("");
    const [password, setPassword] = useState("");
    const [successful, setSuccessful] = useState(false);
    const [message, setMessage] = useState("");
    const [showRegisterFromAdminAuth, setShowRegisterFromAdminAuth] = useState(false)

    const [checkboxGDPR, setCheckboxGDPR] = useState(false);


    useEffect(() => {
        const user = AuthService.getCurrentUser();

        if (user) {
            setShowRegisterFromAdminAuth(user.roles.includes("ROLE_ADMIN"))

        } else {
            setShowRegisterFromAdminAuth(false);
        }
    }, [])


    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangeEmail = (e) => {
        const email = e.target.value;
        setEmail(email);
    };

    const onChangeFirstname = (e) => {
        const firstname = e.target.value;
        setFirstname(firstname);
    };

    const onChangeLastname = (e) => {
        const lastname = e.target.value;
        setLastname(lastname);
    };

    const onChangePhone = (e) => {
        const phone = e.target.value;
        setPhone(phone);
    };

    const onChangeAddress = (e) => {
        const address = e.target.value;
        setAddress(address);
    };

    const onChangePostalcode = (e) => {
        const postalcode = e.target.value;
        setPostalcode(postalcode);
    };

    const onChangeCity = (e) => {
        const city = e.target.value;
        setCity(city);
    };

    const onChangeRole = (e) => {
        const role = e.target.value;
        setRole(role);
    };
    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };


    const onChangeCheckbox = () => {
        if (checkboxGDPR === false) {
            setCheckboxGDPR(true);

        } else {
            setCheckboxGDPR(false);
        }
        document.getElementById("btnSubmit").disabled = checkboxGDPR;
    }


    const handleRegister = (e) => {
        e.preventDefault();

        setMessage("");
        setSuccessful(false);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {
            AuthService.register(username, email, firstname, lastname, phone, address, postalcode, city, password, role).then(
                (response) => {
                    setMessage(response.data.message);
                    setSuccessful(true);
                },
                (error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessage(resMessage);
                    setSuccessful(false);
                }
            );
        }
    };

    return (
        <div className="mt-4 w-100 justify-content-center d-flex ">
            <div class="d-flex w-25 border rounded justify-content-center">
                <div class="d-flex flex-column py-4 ">
                    <div class="d-flex justify-content-center mb-3"><h1>Registrering</h1></div>
                    <Form onSubmit={handleRegister} ref={form}>
                        {!successful && (
                            <div class="mx-2">
                                <div className="d-flex flex-row ">
                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="username">Användarnamn</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="username"
                                            value={username}
                                            onChange={onChangeUsername}
                                            validations={[required, vusername]}
                                        />
                                    </div>

                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="email">Email</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="email"
                                            value={email}
                                            onChange={onChangeEmail}
                                            validations={[required, validEmail]}
                                        />
                                    </div>
                                </div>

                                <div className="d-flex flex-row ">
                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="firstname">Förnamn</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="firstname"
                                            value={firstname}
                                            onChange={onChangeFirstname}
                                        />
                                    </div>
                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="lastname">Efternamn</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="lastname"
                                            value={lastname}
                                            onChange={onChangeLastname}
                                        />
                                    </div>
                                </div>

                                <div className="d-flex flex-row">
                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="phone">Telefonnummer</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="phone"
                                            value={phone}
                                            onChange={onChangePhone}
                                        />
                                    </div>
                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="address">Adress</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="address"
                                            value={address}
                                            onChange={onChangeAddress}
                                        />
                                    </div>
                                </div>

                                <div className="d-flex flex-row">
                                    <div className="form-group mb-2 d-flex d-flex flex-column w-50 mx-3">
                                        <label htmlFor="postalcode">Postnr.</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="postalcode"
                                            value={postalcode}
                                            onChange={onChangePostalcode}
                                        />
                                    </div>
                                    <div className="form-group mb-2 d-flex flex-column w-50 mx-3">
                                        <label htmlFor="city">City</label>
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="city"
                                            value={city}
                                            onChange={onChangeCity}
                                        />
                                    </div>
                                </div>

                                <div className="d-flex flex-row">
                                    <div className="form-group mb-4 d-flex w-50 mx-3 flex-column ">
                                        <label htmlFor="password">Lösenord</label>
                                        <Input
                                            type="password"
                                            className="form-control"
                                            name="password"
                                            value={password}
                                            onChange={onChangePassword}
                                            validations={[required, vpassword]}
                                        />
                                    </div>

                                    {showRegisterFromAdminAuth ? (
                                        <div className="form-group mb-4 d-flex w-50 mx-3 flex-column">
                                            <label htmlFor="role">Roll</label>
                                            <select
                                                className="user_role form-control"
                                                onChange={onChangeRole}
                                                value={role}
                                            >
                                                <option value={"customer"}>Kund</option>
                                                <option value={"employee"}>Anställd</option>
                                                <option value={"admin"}>Admin</option>
                                            </select>
                                        </div>
                                    ) : (
                                        <div className="form-group mb-4 d-flex w-50 flex-column">
                                        </div>
                                    )}
                                </div>
                                <div className={"text-center"}>
                                    <label className="form-check-label mb-3 mx-3" htmlFor="checkboxGDPR">
                                        Jag accepterar <a href="https://gdpr-info.eu/" target="_blank" rel={"noreferrer"}>GDPR</a>
                                    </label>
                                    <input
                                        className="form-check-input"
                                        type="checkbox"
                                        value=""
                                        id="checkboxGDPR"
                                        onChange={onChangeCheckbox}
                                    />
                                </div>
                                <div className="form-group d-flex mx-5">
                                    <button
                                        className="btn btn-primary btn-block"
                                        id="btnSubmit"
                                        disabled>
                                        Skapa konto
                                    </button>
                                </div>
                            </div>
                        )}

                        {message && (
                            <div className="form-group">
                                <div
                                    className={successful ? "alert alert-success" : "alert alert-danger"}
                                    role="alert" >
                                    {message}
                                </div>
                            </div>
                        )}
                        <CheckButton style={{ display: "none" }} ref={checkBtn} />
                    </Form>
                </div>
                </div>

        </div>
    );
};

export default RegisterUser;