import React, {useEffect, useState} from 'react';
import UserService from "../../services/user.service";
import esp5 from "../../resources/DammTussen256x256sponge.png";

const CleaningCard = () => {
    const [cleaningCard, setCleaningCard] = useState([]);

    useEffect(() => {
        getCleaningCard()
    }, [])

    const getCleaningCard = () => {
        UserService.getCleaningServices().then((response) => {
            setCleaningCard(response.data)
        });
    };

    return (
        <div className="d-flex flex-row mt-3">
            {cleaningCard.map(item =>
                <div className="card text-center d-flex mx-3" style={{"width" : "200px", "height" : "268px"}} key={item.id}>
                    <div className="card-header bg-info ">{item.name}</div>
                    <div className="d-flex flex-row justify-content-center pt-3">
                        <p className="d-flex mx-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam finibus.</p>
                    </div>
                    <div className="card-footer text-muted">
                        <div className="d-flex flex-row justify-content-center">
                            <p className="d-flex mx-1">Pris:</p>
                            <div className="d-flex flex-row">
                                <div  className="d-flex mx-1">{item.price}</div>
                                <p className="d-flex">kr</p>
                            </div>
                        </div>
                        <div className="d-flex flex-row justify-content-center">
                            <p className="d-flex mx-1">Rut: </p>
                            <div className="d-flex flex-row">
                                <div className="d-flex mx-1">{item.msrp}</div>
                                <p className="d-flex">kr</p>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default CleaningCard;