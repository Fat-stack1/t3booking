import React from "react";

const NotFound = () => {
    return (
        <div>
            <div>
                <strong>
                    404 Not Found
                </strong>
                <button>Go back to start page</button>
            </div>
        </div>
    )
}

export default NotFound;