import React, { useState, useEffect } from "react";

import UserService from "../../services/user.service";
import CleaningCard from "./CleaningCard";
import ImageCredits from "./ImageCredits";

const Home = () => {
    const [content, setContent] = useState("");

    useEffect(() => {
        UserService.getPublicContent().then(
            (response) => {
                setContent(response.data);
            },
            (error) => {
                const _content =
                    (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                setContent(_content);
            }
        );
    }, []);

    return (
        <div className="container">
            <header className="jumbotron">
                <h3 class="text-center mt-5">{content}</h3>
            </header>

            <div className="d-flex flex-column justify-content-center align-items-center my-4">
                <div className="d-flex"><p>Ny hos oss? Registrera dig här: </p></div>
                <div className="d-flex">
                    <a className="btn btn-info" type="button" href={"/register"} style={{textDecoration: 'none'}}>Ny kund</a>
                </div>
            </div>
            <div className="d-flex flex-column justify-content-center align-items-center">
                <h2 className="d-flex">Vårt utbud av städtjänster:</h2>
                <ul className="d-flex flex-column justify-content-center">
                    <CleaningCard/>
                </ul>
            </div>
            <hr className={"text-dark"}/>
            <div className="d-flex flex-column justify-content-center align-items-center">
                <ImageCredits/>
            </div>
        </div>
    );
};

export default Home;