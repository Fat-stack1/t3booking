import React, {useState, useEffect} from "react";
import {Link, Redirect} from "react-router-dom";

import AuthService from "../../services/auth.service";
import EventBus from "../../common/EventBus";
import logo from "../../resources/DammTussenDammig.ico";
import BoardAdmin from "../BoardAdmin";
import InfoIcon from "@material-ui/icons/InfoTwoTone";

const NavBar = (props) => {
    const {role} = props;
    const [currentUser, setCurrentUser] = useState(AuthService.getCurrentUser);

    const [showCustomerBoard, setShowCustomerBoard] = useState(false);
    const [showEmployeeBoard, setShowEmployeeBoard] = useState(false);
    const [showAdminBoard, setShowAdminBoard] = useState(false);

    useEffect(() => {
        setCurrentUser(AuthService.getCurrentUser());
        if (currentUser && role === "admin") {
            handleAdminView();
        }
        if (currentUser && role === "customer") {
            handleEmployeeView();
        }
        if (currentUser && role === "employee") {
            handleCustomerView();
        }
        if(!currentUser){
            return (<Redirect to={"/"}/>);
        }
        EventBus.on("logout", () => {
            logOut();
        });

        return () => {
            EventBus.remove("logout");
            window.location.reload();
        };
    }, []);

    const handleAdminView = () => {
        setShowAdminBoard(true);
        setShowCustomerBoard(false);
        setShowEmployeeBoard(false);
    }

    const handleEmployeeView = () => {
        setShowAdminBoard(false);
        setShowCustomerBoard(false);
        setShowEmployeeBoard(true);
    }

    const handleCustomerView = () => {
        setShowAdminBoard(false);
        setShowCustomerBoard(true);
        setShowEmployeeBoard(false);
    }

    const logOut = () => {
        AuthService.logout();
        setShowEmployeeBoard(false);
        setShowCustomerBoard(false);
        setShowAdminBoard(false);
        setCurrentUser(undefined);
    };

    return(
        <nav className="navbar navbar-expand navbar-dark bg-dark ">
            <btn href={"https://www.artstation.com/elsapatel"}>
                <img className="mx-4" style={{"width" : "64px", "height" : "64px"}} src={logo} alt="Dammtussen-logo"/>
            </btn>
            <Link to={"#"} className="navbar-brand">
                <h1 className="my-4">Städa Fint AB</h1>
            </Link>
            <div className="navbar-nav mr-auto">
                {currentUser && (
                    <div className="navbar-nav ml-auto">
                        {/*<li className="nav-item">*/}
                        {/*    <Link to={`/${role}`} className="nav-link">*/}
                        {/*        <i>{currentUser.username}</i>*/}
                        {/*    </Link>*/}
                        {/*</li>*/}
                        <li className="nav-item mx-3">
                            <Link to="/login" className="nav-link" onClick={logOut}>
                                LOGGA UT
                            </Link>
                        </li>
                    </div>
                )}
            </div>
        </nav>
    )
}
export default NavBar;