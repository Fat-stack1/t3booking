import React from "react";
import esp572 from "../../resources/esp-572.png";
import esp5b from "../../resources/esp-5-b.png";
import esp571 from "../../resources/esp-5.7.1.png";
import esp5d from "../../resources/esp-5-d.png";
import esp5 from "../../resources/DammTussen256x256sponge.png";

const ImageCredits = () => {
    return(
        <div className={"d-flex flex-column"}>
            <div>Cred och stort tack till grafiker <strong className={"text-dark"}><i>Elsa Söderpalm Patel</i></strong>  för bidraget med designen av våra ikoner och loggan. Mer av hennes art hittas på hennes  <strong><a href={"https://www.artstation.com/elsapatel"} className={"text-decoration-none text-info"}> artstation</a></strong>!</div>
            <div className={"d-flex flex-row justify-content-center mx-5 mb-5"}>
                <div className="d-flex flex-row mt-3">
                    {/*<div className="card text-center d-flex mx-3" style={{"width" : "200px", "height" : "268px"}}>*/}
                    <div className="card-header bg-secondary ">
                        <img className="d-flex mx-2 text-center" style={{ "width": "128px", "height": "128px" }} src={esp572} alt={"Artist: Elsa Söderpalm Patel"}/>
                    </div>
                    {/*</div>*/}
                </div>
                <div className="d-flex flex-row mt-3">
                    {/*<div className="card text-center d-flex mx-3" style={{"width" : "200px", "height" : "268px"}}>*/}
                    <div className="card-header bg-info ">
                        <img className="d-flex mx-2 text-center" style={{ "width": "128px", "height": "128px" }} src={esp5b} alt={"Artist: Elsa Söderpalm Patel"}/>
                    </div>
                    {/*</div>*/}
                </div>
                <div className="d-flex flex-row mt-3">
                    {/*<div className="card text-center d-flex mx-3" style={{"width" : "200px", "height" : "268px"}}>*/}
                    <div className="card-header bg-secondary ">
                        <img className="d-flex mx-2 text-center" style={{ "width": "128px", "height": "128px" }} src={esp571} alt={"Artist: Elsa Söderpalm Patel"}/>
                    </div>
                    {/*</div>*/}
                </div>
                <div className="d-flex flex-row mt-3">
                    {/*<div className="card text-center d-flex mx-3" style={{"width" : "200px", "height" : "268px"}}>*/}
                    <div className="card-header bg-info ">
                        <img className="d-flex mx-2 text-center" style={{ "width": "128px", "height": "128px" }} src={esp5d} alt={"Artist: Elsa Söderpalm Patel"}/>
                    </div>
                    {/*</div>*/}
                </div>
                <div className="d-flex flex-row mt-3">
                    {/*<div className="card text-center d-flex mx-3" style={{"width" : "200px", "height" : "268px"}}>*/}
                    <div className="card-header bg-secondary ">
                        <img className="d-flex mx-2 text-center" style={{ "width": "128px", "height": "128px" }} src={esp5} alt={"Artist: Elsa Söderpalm Patel"}/>
                    </div>
                    {/*</div>*/}
                </div>
            </div>
        </div>
    )
}

export default ImageCredits;