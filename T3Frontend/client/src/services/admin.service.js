import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/t3/';

// Get all users
const getAllUsers = () => {
    return axios.get(API_URL + 'users', {
        headers: authHeader()
    });
}

// Manage Employees
const getEmployeesList = () => {
    return axios.get(API_URL + 'employees', {
        headers: authHeader()
    });
}

// Manage Customers
const getCustomersList = () => {
    return axios.get(API_URL + 'customers', {
        headers: authHeader()
    });
}

// Manage ONE user
const getUserById = (id) => {
    return axios.get(`${API_URL}users/${id}`, {
        headers: authHeader()
    });
};

// Update ONE user
const update = (id, data) => {
    return axios.put(`${API_URL}users/${id}`, data, {
        headers: authHeader()
    });
};

// Delete ONE user
const remove = (id) => {
    return axios.delete(`${API_URL}users/${id}`, {
        headers: authHeader()
    });
};

// Delete ALL users
const removeAll = () => {
    return axios.delete(`${API_URL}users`, {
        headers: authHeader()
    });
};

// Find user by role
const findByRole = role => {
    return axios.get(`${API_URL}/users?role=${role}`, {
        headers: authHeader()
    });
};

export default {
    getAllUsers,
    getEmployeesList,
    getCustomersList,
    getUserById,
    update,
    remove,
    removeAll,
    findByRole
};