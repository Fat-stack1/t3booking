import axios from "axios";

const API_URL = "http://localhost:8080/api/auth/";

const register = (username, email, firstname, lastname, phone, address, postalcode, city, password, role) => {
    return axios.post(API_URL + "signup", {
        username,
        email,
        firstname,
        lastname,
        phone,
        address,
        postalcode,
        city,
        password,
        role : [role]
    });
};

const login = (username, password) => {
    return axios
        .post(API_URL + "sign-in", {
            username,
            password,
        })
        .then((response) => {
            if (response.data.token) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }

            return response.data;
        });
};

const logout = () => {
    localStorage.removeItem("user");
};

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

export default {
    register,
    login,
    logout,
    getCurrentUser,
};