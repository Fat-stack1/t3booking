import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:8080/api/t3/";

const getBookings = () => {
    return axios.get(API_URL + 'bookings', {
        headers: authHeader()
    });
};

const getBookingById = (id) => {
    return axios.get(`${API_URL}bookings/${id}`, {
        headers: authHeader()
    });
};

const createBooking = (customer_id, cleaning_service_id, date, time, details) => {
    return axios.post(API_URL + "addbooking", {
        headers : authHeader(),
        customer_id : customer_id,
        cleaning_service_id : cleaning_service_id,
        date : date,
        time : time,
        details : details
        })
        .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
};

const update = (id, data) => {
    return axios.put(`${API_URL}bookings/${id}`, data, {
        headers: authHeader()
    });
};

const remove = (id) => {
    return axios.delete(`${API_URL}bookings/${id}`, {
        headers: authHeader()
    });
};

const removeAll = () => {
    return axios.delete(`${API_URL}bookings`, {
        headers: authHeader()
    });
};

// BY USER_ID
const getBookingByCustomerId = (customer_id) => {
    return axios.get(`${API_URL}customerbookings/${customer_id}`, {
        headers: authHeader()
    });
};

const getBookingByEmployeeId = (employee_id) => {
    return axios.get(`${API_URL}employeebookings/${employee_id}`, {
        headers: authHeader()
    });
};

// BY STATUS
const getAdminBookingByStatus = (status) => {
    return axios.get(`${API_URL}bookings/admin-status?status=${status}`, {
        headers: authHeader()
    });
};

const getCustomerBookingByStatus = (customer_id, status) => {
    return axios.get(`${API_URL}bookings/customer-history?customer_id=${customer_id}&status=${status}`, {
        headers: authHeader()
    });
};
const getEmployeeBookingByStatus = (employee_id, status) => {
    return axios.get(`${API_URL}bookings/employee-history?employee_id=${employee_id}&status=${status}`, {
        headers: authHeader()
    });
};

export default {
    getBookings,
    getBookingById,
    createBooking,
    update,
    remove,
    removeAll,
    getBookingByCustomerId,
    getBookingByEmployeeId,
    getCustomerBookingByStatus,
    getEmployeeBookingByStatus,
    getAdminBookingByStatus
};