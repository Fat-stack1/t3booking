import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/t3/';

// Home page content
const getPublicContent = () => {
    return axios.get(API_URL + 'all');
};

// List of all cleaning services
const getCleaningServices = () => {
    return axios.get(API_URL + 'cleanings', {
        headers: authHeader()
    });
};

// DELETE profile
const deleteProfile = (id) => {
    return axios.delete(API_URL + `profile/${id}`, {
            headers: authHeader()
        });
};

// CUSTOMER
const getBookingBoard = () => {
    return axios.get(API_URL + 'user', {
        headers: authHeader()
    });
};

// CUSTOMER-INFO
const getCustomerBoard = () => {
    return axios.get(API_URL + 'customer', {
        headers: authHeader()
    });
};

// EMPLOYEE-INFO
const getEmployeeBoard = () => {
    return axios.get(API_URL + 'employee', {
        headers: authHeader()
    });
};

// ADMIN-INFO
const getAdminBoard = () => {
    return axios.get(API_URL + 'admin', {
        headers: authHeader()
    });
};

export default {
    getPublicContent,
    getEmployeeBoard,
    getCustomerBoard,
    getAdminBoard,
    getBookingBoard,
    getCleaningServices,
    deleteProfile
};